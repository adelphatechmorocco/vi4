( function( $ ) {
    if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
        // Firefox 38+ seems having support of enumerateDevicesx
        navigator.enumerateDevices = function(callback) {
            navigator.mediaDevices.enumerateDevices().then(callback);
        };
    } else{
        window.location.href = Routing.generate('browser_not_updated');
    }
    
    var MediaDevices = [];
    var isHTTPs = location.protocol === 'https:';
    var canEnumerate = false;
    
    if (typeof MediaStreamTrack !== 'undefined' && 'getSources' in MediaStreamTrack) {
        canEnumerate = true;
    } else if (navigator.mediaDevices && !!navigator.mediaDevices.enumerateDevices) {
        canEnumerate = true;
    }
    
    var hasMicrophone = false;
    var hasSpeakers = false;
    var hasWebcam = false;
    
    var isMicrophoneAlreadyCaptured = false;
    var isWebcamAlreadyCaptured = false;
    
    function checkDeviceSupport(callback) {
        if (!canEnumerate) {
            return;
        }
    
        if (!navigator.enumerateDevices && window.MediaStreamTrack && window.MediaStreamTrack.getSources) {
            navigator.enumerateDevices = window.MediaStreamTrack.getSources.bind(window.MediaStreamTrack);
        }
    
        if (!navigator.enumerateDevices && navigator.enumerateDevices) {
            navigator.enumerateDevices = navigator.enumerateDevices.bind(navigator);
        }
    
        if (!navigator.enumerateDevices) {
            if (callback) {
                callback();
            }
            return;
        }
    
        MediaDevices = [];
        navigator.enumerateDevices(function(devices) {
            devices.forEach(function(_device) {
    
                var device = {};
                for (var d in _device) {
                    device[d] = _device[d];
                }
    
                if (device.kind === 'audio') {
                    device.kind = 'audioinput';
                }
    
                if (device.kind === 'video') {
                    device.kind = 'videoinput';
                }
    
                var skip;
                MediaDevices.forEach(function(d) {
                    if (d.id === device.id && d.kind === device.kind) {
                        skip = true;
                    }
                });
    
                if (skip) {
                    return;
                }
    
                if (!device.deviceId) {
                    device.deviceId = device.id;
                }
    
                if (!device.id) {
                    device.id = device.deviceId;
                }
    
                if (!device.label) {
                    device.label = 'Please invoke getUserMedia once.';
                    if (!isHTTPs) {
                        device.label = 'HTTPs is required to get label of this ' + device.kind + ' device.';
                    }
                } else {
                    if (device.kind === 'videoinput' && !isWebcamAlreadyCaptured) {
                        isWebcamAlreadyCaptured = true;
                    }
    
                    if (device.kind === 'audioinput' && !isMicrophoneAlreadyCaptured) {
                        isMicrophoneAlreadyCaptured = true;
                    }
                }
    
                if (device.kind === 'audioinput') {
                    hasMicrophone = true;
                }
    
                if (device.kind === 'audiooutput') {
                    hasSpeakers = true;
                }
    
                if (device.kind === 'videoinput') {
                    hasWebcam = true;
                }
    
                // there is no 'videoouput' in the spec.
                MediaDevices.push(device);
            });
    
            if (callback) {
                callback();
            }
        });
    }
    
    checkDeviceSupport(function() {
        var micro = false;
        var webcam = false;
    
        $('#test-video').on('click', function(){
            if(hasWebcam == true){
                $('#test-video').removeClass('no-test');
                $('.video-test').addClass('passed');
                $('#test-video').addClass('passed');
                $('#test-video').html("passed!");
            webcam = true;
            if(micro == true){
                $('#begin-interview-link').removeAttr('class');
                $('#begin-interview-link').addClass('active');
            }
        }else{
            valid = false;
            $('.solid-color-alert').html('<div class="alert red-solid-alert">\
                                                <div class="icon-alert"><i class="fa fa-times" aria-hidden="true">\
                                                </i></div><div class="text-solid-alert"><h5>warning</h5>\
                                                <span>Camera not detected.</span>\
                                                </div>\
                                                </div>');
        }
        });
            $('#btn-test-audio').on('click', function(){
                $("#modal-test-audio").modal('show');
                if(hasMicrophone == true){
                $('.audio-test').removeClass('no-test');
                $('.audio-test').addClass('passed');
                $('#btn-test-audio').addClass('passed');
                $('#btn-test-audio').removeClass('no-test');
                $('#btn-test-audio').html('passed');
    
                $( ".testing-audio .vjs-device-button" ).trigger( "click" );
                $( ".testing-audio .vjs-record-button" ).trigger( "click" );
                micro = true;
                if(webcam == true){
                     $('#begin-interview-link').addClass('active');
                }
            }else{
               $('.solid-color-alert').html('<div class="alert red-solid-alert">\
                                                    <div class="icon-alert"><i class="fa fa-times" aria-hidden="true">\
                                                    </i></div><div class="text-solid-alert"><h5>warning</h5>\
                                                    <span>Microphone not detected.</span>\
                                                    </div>\
                                                    </div>');
             }
            });
    });
    
    
    var player = videojs("audio-test",
    {
        controls: false,
        width: 400,
        height: 200,
        plugins: {
            wavesurfer: {
                src: "live",
                waveColor: "#63ba00",
                progressColor: "#2E732D",
                cursorWidth: 1,
                msDisplayMax: 20,
                hideScrollbar: true
            },
            record: {
                audio: true,
                video: false,
                maxLength: 20,
                debug: false
            }
        }
    });
    
    
    player.recorder.enumerateDevices();
    player.on('enumerateReady', function()
    {
        var devices = player.recorder.devices;
         var nbCamera = 0;
         var i = 1;
                devices.forEach(function(_device) {
                   if (_device.kind == "videoinput") {
                       var label = 'Camera '+ i;
                       if (_device.label) {
                           label = _device.label;
                       }
                       $('ul#list-choose-camera').append('<li><a class="camera" id="'+ _device.deviceId +'" href="#">'+ label +'</a></li>');
                       nbCamera ++;
                   }
                   i++;
                });
                if (nbCamera == 0){
                       $('.list-cameras').append('<div class="no-camera">Camera no detected</div>');
                }
    
    });
    
    $('body').on('click' , 'a.camera', function(e){
        e.preventDefault();
    
        var idCamera = $(this).attr('id');
        $('#sourceId').val(idCamera);
        var player = videojs("video-test",
    {
        controls: false,
        nativeControlsForTouch: false,
        preload: "auto",
        width: 680,
        height: 440,
        plugins: {
            record: {
                audio: true,
                video: {
                    optional: [{
                    sourceId: idCamera
                    }]
                },
                maxLength: 10,
                debug: false,
            }
        }
    });
    
    $( ".testing-video .vjs-device-button" ).trigger( "click" );
    $( ".testing-video .vjs-record-button" ).trigger( "click" );
    $('#test-video').removeAttr('data-target');
    $('#modal-list-cameras').modal('hide');
    });
    
    })(jQuery);