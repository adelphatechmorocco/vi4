$( document ).ready(function() {
var duration = $('#video-question').data('duration');
var cameraId = $('#sourceId').val();
var player = videojs("video-question",
{
    controls: true,
    nativeControlsForTouch: false,
    preload: "auto",
    width: 520,
    height: 340,
    plugins: {
        record: {
            audio: true,
            video: {
                optional: [{
                sourceId: cameraId
                }]
            },
            maxLength: duration,
            debug: true,

        }
    }
});
// error handling
player.on('deviceError', function()
{
    console.log('device error:', player.deviceErrorCode);
});

// user clicked the record button and started recording
player.on('startRecord', function()
{

    $('.duration-non-start').css('display', 'none');
    $('#duration').startTimer();
    $('.video-js .vjs-control-bar').show();

    /* active button recording */
    $('#next-question').removeAttr('disabled');
    $('#next-question').addClass('active');
    $('.buttom-video').css('display','block');
    /* BYE button recording */
    console.log('started recording!<br>');

});

// user completed recording and stream is available
player.on('finishRecord', function()
{
    console.log(player);
    console.log(player.recordedData.video);
    // the blob object contains the recorded data that
    // can be downloaded by the user, stored on server etc.
    console.log('finished recording');
    $('.video-js .vjs-control-bar').hide();
    $('#duration').trigger('complete');
    $('#next-question').removeAttr('disabled');
    $('#next-question').addClass('active');
    //$('.answer-video').css('display', 'block');
});


$('#next-question').on('click', function(e){
    e.preventDefault();
    $('.vjs-icon-record-stop').trigger('click');
    //$(this).trigger('click');
    setTimeout(function(){
        var token = $('.container-video').data('token');
    var writtenAnswer = $('#applicant_video_writtenAnswer').val();

    var data = new FormData();
    var blobFile;
    if (typeof player.recordedData.video === 'undefined') {
     blobFile = player.recordedData;
    }else{
        blobFile = player.recordedData.video;
    }

    data.append('applicant_video[writtenAnswer]', writtenAnswer);
    data.append('applicant_video[videoFile][file]', blobFile);
    $('#global-interview-video').html('<div class="loader-video">Loading...</div>');
     $.ajax({
       url : Routing.generate('applicant_interview_question', { token: token}),
       type: 'POST',
       data: data,
       contentType: false,
       processData: false,
       success: function(rs) {
        player.dispose();
        if (rs.notNextVideo) {
            $('#global-interview-video').html(rs.thankyouContent);

        } else{
            $('.vjs-device-button').trigger( "click" );
            $('.vjs-record-button').trigger('click');
            $('#global-interview-video').html(rs.question);
            $('#applicant_video_writtenAnswer').val('');
            }
       },
       error: function(error) {
        console.log(error);
       }
     });

    }, 100);

});


});
