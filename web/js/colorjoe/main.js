fixScale(document);

main();


function main() {
    var defaultColor = '#ffffff';
    var colorJob = document.getElementById('create_job_setup_branding_backgroundColorPreview').value;
    if (colorJob){
        defaultColor = colorJob;
    }
   
    var joe = colorjoe.rgb('extraPicker', defaultColor, [
        'currentColor',
        ['fields', {space: 'RGB', limit: 255, fix: 2}],
        ['fields', {space: 'HSLA', limit: 100}],
        ['fields', {space: 'CMYKA', limit: 100}],
        'hex',
        'text',
    ]);

    joe.on('change', function(color) {
        $('#create_job_setup_branding_backgroundColorPreview').val(color.hex());
        $(this).prop('checked', false);
        $(".bg-texture input:radio").attr("checked", false);
    });
}
