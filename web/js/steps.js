jQuery( document ).ready(function( $ ) {
    
        // STEP 3
if ($(".step-3-upload-logo ")[0]){

    $('.step-3-upload-logo input[type="file"]').ezdz({
        classes: {
        main:      'file-dropzone',
        enter:     'file-enter',
        reject:    'file-reject',
        accept:    'file-accept',
        focus:     'file-focus'
        },
        accept: function(file) {
        console.log(file);
        $('.container-logo-detailles').css('display', 'block');
        var imgSrc = $('.file-dropzone div img').attr('src');
        var size = (file.size) / 1000;
        var fileName = file.name;

        var day = file.lastModifiedDate.getDay(file.lastModifiedDate);
        var month = file.lastModifiedDate.getMonth(file.lastModifiedDate);
        var full_year = file.lastModifiedDate.getFullYear(file.lastModifiedDate);

        $('.detailles-logo strong').text(fileName);
        $('.detailles-logo .size').text(Math.round(size)+'Ko');
        $('.detailles-logo .date').text(day +"/" + month +"/" + full_year);

        $('.uploaded-logo .img-logo img').attr('src' ,imgSrc);
    },});

    $('.btn-delete-logo').on('click' , function(e){
        e.preventDefault();
        $('.step-3-upload-logo input[type="file"]').val("");
        $('#create_job_setup_branding_companyLogoFile').val('');
        $('.uploaded-logo .img-logo img').attr('src' ,'');
        $('.detailles-logo strong').text('');
        $('.detailles-logo .size').text('');
        $('.detailles-logo .date').text('');
        $('.container-logo-detailles').css('display', 'none');
        
    });
    $('.btn-upload-logo').on('click' , function(e){
        e.preventDefault();
        $('#create_job_setup_branding_companyLogoFilePreview').trigger("click");
    });
}
//SECOND STEP crate video

//LAST STEP
if ($("#create_job_links_links")[0]){
var prototype = $('#create_job_links_links').attr('data-prototype');
            $('.input-add-new-link .input-add-link').append(prototype);

            $('#add-new-link').on('click' , function(e) {
                var newlink = '';
                e.preventDefault();
                var label = $('.input-add-new-link input[type="text"]').val();
                var jobid = $(this).data('jobid');
                   $.ajax({
                        url: Routing.generate('job_add_shortner', { id: jobid}),
                        type: 'POST',
                        data: {  label: label },
                        success: function(rs) {
                            
                                var liencurrent = window.location.href;
                                var link = "";
                                var copy = "";
             
                if (liencurrent.match(/en/)) {
                    link = '<div class="btn-add-link"><button type="submit" class="btn" disabled>Create link</button></div> ';
                    copy = '<button id="'+ rs.slug +'" type="button" class="button-copy-link">Copy</button>';
                 
                 }else{
                     link = '<div class="btn-add-link"><button type="submit" class="btn" disabled>Créer un lien</button></div> ';
                    copy = '<button id="'+ rs.slug +'" type="button" class="button-copy-link">copie</button>';
                     
                 }

                                $('.input-add-new-link input[type="text"]').val('');
                                var content_form = '<div class="new-link-share link-share">' +
                                '<div class="form-group-inline"> ' +
                                  link +
                                '<div class="input-add-link">' +
                                '<input disabled type="text" class="form-control create-new-link" value="' + label + '" > ' +
                                '</div> ' +
                                '<div class="show-new-link">' +
                                '<label for="show-new-link">link</label>' +
                                '<input type="text" id="'+ rs.slug +'" value="' + rs.shortener +'"  class="copy-new-link" disabled=""> ' +
                                '</div>' +
                                '<div class="btn-copy-link"> ' +
                                copy +
                                '</div> </div>' +
                                '</div>';

                                $('.create-new-link-share').append(content_form);
                                $(".button-copy-link").trigger('click');
                                $('.button-copy-link').popover('hide');
                                $(body).removeClass('stop-scrolling');
                        },

                });

});
}
$( "#questionsVideoList" ).sortable({
    placeholder: 'ui-state-highlight',
    items: '.row-video',
    start: function(event, ui) {
         $(this).addClass('active-sort'); 
    },
     update: function() {
        var order = $(this).sortable('toArray' , {
            attribute: "id"
        });
       var slug = $('#questionsVideoList').data('slug');
       $.each( order, function( key, value ) {
                      $('#'+ value + ' .nb').html(key+1);
                    })
        $.ajax({
            type: "POST",
            url: Routing.generate('job_question_video_order', {'slug':slug}),
            data: {order:order},

        }).done(function(rs) {
            $('#questionsVideoList').html(rs.newList);
        });
                    
      }
});
/* Add new question video*/
if ($("#save-question")[0]){
$('form[name="question_video_questionsVideo_create"]').submit(function(e){
    e.preventDefault();
    var slug = $('#questionsVideoList').data('slug');
    var question = $('#question_video_questionsVideo___name___question').val();
    var duration = $('#question_video_questionsVideo___name___duration').val();
    var timeReadQuestion = $('#question_video_questionsVideo___name___timeReadQuestion').val();
    $.ajax({
        type: 'POST',
        url: Routing.generate('job_question_video_add', { slug: slug}),
        data: {question:question, duration:duration, timeReadQuestion:timeReadQuestion},
        success: function(rs){
           $('#question_video_questionsVideo___name___question').val('');
            $('#questionsVideoList').html(rs.listQuestionsVideo);
            $('#question-rest').html(5 - rs.count);
            if (rs.count >= 5) {
                $('#add-new-question-video').hide();
            }

             
                var liencurrent = window.location.href;
             
                if (liencurrent.match(/en/)) {
                    swal({
                    title: 'Good job!',
                    text: 'Update with successfully',
                    type: 'success',
                    html: true,
                    timer: 1000
                    });
                 
                 }else{
                     
                    swal({
                    title: 'Bon travail!',
                    text: 'Ajouté avec succès',
                    type: 'success',
                    html: true,
                    timer: 1000
                    }); 
                 }

        }
    });
    });
}

/* DELETE QUESTION VIDEO */
    $("#questionsVideoList").on("click", ".delete-question-video", function(e){
      e.preventDefault(); 
      var id = $(this).attr('id');
      var slug = $('#questionsVideoList').data('slug');
      swal({  
        title: "Are you sure?",
        text: "Do you really want to make the deletion!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "##a30101",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,    
     }, 
        function(){
             setTimeout(function(){
               $.ajax({
                    type: 'POST',
                    url: Routing.generate('question_video_delete', { id: id, slug: slug}),
                    success: function(rs){
                        console.log(rs.count);
                        $('#questionsVideoList').html(rs.listQuestionsVideo);
                        $('#question-rest').html(5 - rs.count);
                        if (rs.count < 5) {
                            $('#add-new-question-video').css('display', 'block').removeClass('add-new-question-video-hide');
                        }

                var liencurrent = window.location.href;
             
                if (liencurrent.match(/en/)) {
                    swal({
                    title: 'Good job!',
                    text: 'Update with successfully',
                    type: 'success',
                    html: true,
                    timer: 1000
                    });
                 
                 }else{
                     
                    swal({
                    title: 'Bon travail!',
                    text: 'Supprimer avec succès',
                    type: 'success',
                    html: true,
                    timer: 1000
                    }); 
                 }

                    }
                });
             });
         });
   
  });  

$("body").on("click", ".button-copy-link", function(e){

var clipboard = new Clipboard('.button-copy-link', {
        text: function(e) {
            var  selector = e.id;
            var valInput = $('.show-new-link #'+selector).val();
            return valInput;
        }
    });

clipboard.on('success', function(e) {
    var selector = e.trigger.id;
    $('button#'+e.trigger.id).attr('data-content', 'Copied!');
    $('button#'+e.trigger.id).popover({ content: "Copied!", placement:'top', html:true, trigger:'click'}).popover("show");
});

clipboard.on('error', function(e) {
     var selector = e.trigger.id;
    $('button#'+e.trigger.id).attr('data-content', 'Press ⌘+C to copy');
    $('button#'+e.trigger.id).popover({ content: "Press ⌘+C to copy", placement:'top', html:true, trigger:'click'}).popover("show");
});


});
$("body").on("mouseout", ".button-copy-link", function(){
$('.button-copy-link').popover('hide');
});

$('.create-job-step-1 button.btn-step').on('click' , function(e){
e.preventDefault();
var valid = true;
$('.error-message').hide();
$( ':input[required]', 'form[name="create_job"]' ).each( function (key, element) {
       if(!$.trim(element.value).length) {
        var msg = "";
        if($('html')[0].lang == 'en'){
                msg = "This field cannot be left blank";
        }else{
             msg = "Ce champ est obligatoire";
        }
        $(element).addClass('input-error');
        $(element).after('<div class="alert error-message small-alert">\
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>\
                    '+ msg +' </div>');
        valid = false;
     }
     else{
        $(element).removeClass('input-error');
        $(element).next().hide('error-message');
     }
})
if(valid){
    $('form[name="create_job"]').submit();
}
});

$('.create-job-step-3 button.save-contonue').on('click', function(e){
e.preventDefault();
var valid = true;
$( ':input[required]', 'form[name="create_job_setup_branding"]' ).each( function (key, element) {
     if(!$.trim(element.value).length) {
        var msg = "";
        if($('html')[0].lang == 'en'){
                msg = "This field cannot be left blank";
        }else{
             msg = "Ce champ est obligatoire";
        }
        $('.file-dropzone').addClass('input-error');
        $('.file-dropzone').before('<div class="alert error-message small-alert">\
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>\
                    '+ msg +' </div>');
        valid = false;
     }
      else{
        $(element).removeClass('input-error');
        $(element).prev().hide('error-message');
     }
});
if(valid){
    $('form[name="create_job_setup_branding"]').submit();
}
});


});

$('#cancel-preview-question-video').on('click', function(e){
 e.preventDefault();
 $('.preview-questions').hide();
 $('.create-video-questions').show();
 $('#cancel-preview-question-video').hide();
 $('#preview-question-video').show();

});
/* ============================== PREVIEW QUESTION VIDEO ==============================*/
$('#preview-question-video').on('click' , function(e){
    e.preventDefault();
   var slug = $('#questionsVideoList').data('slug');
   $.ajax({
       url :  Routing.generate('job_preview_question_video', { slug: slug}),
       type: 'POST',
       data: {},
       success: function(rs){
           $('#list-preview-question').empty();
           if (rs.questionsVideo){
            $('.create-video-questions').hide();
            $('#list-preview-question').html(rs.questionsVideo);
            $('.preview-questions').show();
            $('#preview-question-video').hide();
            $('#cancel-preview-question-video').show();
           }
       }
   });
});
/* ============================ END PREVIEW QUESTION VIDEO ============================*/

/**** Preview page ***/
$('form[name="create_job_setup_branding"]').on('click', function(e) {
    var form = $(this).closest('form');

    if ($(e.target).is('.preview-page')) {
        e.preventDefault();

        $(form).attr('action', $(form).data('action-preview-url'));
        $(form).attr('target', '_blank');
        $(form).trigger('submit');
        $(form).attr('action', '');
        $(form).removeAttr('target');
    }
});
/****  end Preview page ***/
