( function( $ ) {


if($('html')[0].lang == 'en'){
                jQuery.extend(jQuery.validator.messages, {
    required: "This field is required.",
});
        }else{
            jQuery.extend(jQuery.validator.messages, {
    required: "Ce champ est requis.",
});
        }



	
	var user = detect.parse(navigator.userAgent);
	var safari = user.browser.family;
	if (safari == "Safari") {
		$('.not-converted').addClass('safari');
		$('.message-not-converted').addClass('safari');
	}
	$('[data-trigger="hover"]').popover(
		{ trigger: "hover" }
	);
	 $("body").on("mouseenter", ".text-help", function(e){
		 $('[data-toggle="popover"]').popover(
		{ trigger: "hover" }
	);
	 });
	
	$(".filter-tabs-content").hide();
	$("#filter-btn").click(function(){
		$(this).toggleClass("active-filter");
		$(".filter-tabs-content").slideToggle()
	});

	$('.btn-search , .close-search').on('click' , function(e){
		$('.sb-search').toggleClass('open');
	});

	if ($('.rateyo-applicant-review')[0]) {
			$( ".rateyo-applicant-review" ).each(function( index ) {
		var id = $(this).attr('id');
		var rate = $("#"+id).data('rate');
		if(rate){
		$(".rateyo-applicant-review").rateYo({
						starWidth: "17px",
						normalFill: "#eede16",
						numStars: 5,
						spacing: "1px",
						rating: rate
					});
		}

	});
	}

	/*** Select plan **/
	$('.next-arrow').on('click' , function(e) {
		e.preventDefault();
		if ($('.plan.selected').next('.plan').length) {
			$('.plans').find('input[name="app_user_account_billing[company][pricingModel]"]').removeAttr('checked');

			$('.plan.selected')
				.removeClass('selected')
				.next('.plan')
				.addClass('selected')
				.find('input[name="app_user_account_billing[company][pricingModel]"]')
				.attr('checked', 'checked')
			;
		}
	});
	
	$('.prev-arrow').on('click', function(e) {
		e.preventDefault();

		if ($('.plan.selected').prev('.plan').length) {
			$('.plans').find('input[name="app_user_account_billing[company][pricingModel]"]').removeAttr('checked');

			$('.plan.selected')
				.removeClass('selected')
				.prev('.plan')
				.addClass('selected')
				.find('input[name="app_user_account_billing[company][pricingModel]"]')
				.attr('checked', 'checked')
			;
		}
	});

	if ($('.card-charged').find('input[value="credit_card"]').is(':checked')) {
		$('.infos-card-credit').addClass('open');
	} else {
		$('.infos-paypal').addClass('open');
	}

	$('.card-charged').find('input[value="credit_card"]').change(function() {
		if(this.checked) {
			$('.infos-paypal').removeClass('open');
			$('.infos-card-credit').addClass('open');
		}
	});

	$('.card-charged').find('input[value="paypal"]').change(function() {
		if(this.checked) {
			$('.infos-card-credit').removeClass('open');
			$('.infos-paypal').addClass('open');
		}
	});

	/*** end delect plan **/

	/*clos tabs*/
	var currentTab;
	var composeCount = 0;
//initilize tabs
	$(function () {

		//when ever any tab is clicked this method will be call
		$("#myTab").on("click", "a", function (e) {
			e.preventDefault();

			$(this).tab('show');
			$currentTab = $(this);
		});


		registerComposeButtonEvent();
		registerCloseEvent();
	});

//this method will demonstrate how to add tab dynamically
	function registerComposeButtonEvent() {
		/* just for this demo */
		$('#composeButton').click(function (e) {
			e.preventDefault();

			var tabId = "compose" + composeCount; //this is id on tab content div where the
			composeCount = composeCount + 1; //increment compose count

			$('.nav-tabs').append('<li><a href="#' + tabId + '"><button class="close closeTab" type="button" >×</button>Compose</a></li>');
			$('.tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

			craeteNewTabAndLoadUrl("", "./SamplePage.html", "#" + tabId);

			$(this).tab('show');
			showTab(tabId);
			registerCloseEvent();
		});

	}

//this method will register event on close icon on the tab..
	function registerCloseEvent() {

		$(".closeTab").click(function () {

			//there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
			var tabContentId = $(this).parent().attr("href");
			$(this).parent().parent().remove(); //remove li of tab
			$('#myTab a:last').tab('show'); // Select first tab
			$(tabContentId).remove(); //remove respective tab content

		});
	}

//shows the tab with passed content div id..paramter tabid indicates the div where the content resides
	function showTab(tabId) {
		$('#myTab a[href="#' + tabId + '"]').tab('show');
	}
//return current active tab
	function getCurrentTab() {
		return currentTab;
	}

//This function will create a new tab here and it will load the url content in tab content div.
	function craeteNewTabAndLoadUrl(parms, url, loadDivSelector) {

		$("" + loadDivSelector).load(url, function (response, status, xhr) {
			if (status == "error") {
				var msg = "Sorry but there was an error getting details ! ";
				$("" + loadDivSelector).html(msg + xhr.status + " " + xhr.statusText);
				$("" + loadDivSelector).html("Load Ajax Content Here...");
			}
		});

	}

//this will return element from current tab
//example : if there are two tabs having  textarea with same id or same class name then when $("#someId") whill return both the text area from both tabs
//to take care this situation we need get the element from current tab.
	function getElement(selector) {
		var tabContentId = $currentTab.attr("href");
		return $("" + tabContentId).find("" + selector);

	}


	function removeCurrentTab() {
		var tabContentId = $currentTab.attr("href");
		$currentTab.parent().remove(); //remove li of tab
		$('#myTab a:last').tab('show'); // Select first tab
		$(tabContentId).remove(); //remove respective tab content
	}
	/*end close tab*/
	$('.burger-menu-signup').on('click' , function(){
		if ( $('.navbar-signup').hasClass("ms-open") ) {
			$(this).removeClass("open");
			$('.navbar-signup').removeClass('ms-open');
		} else {
			$('.navbar-signup').addClass('ms-open');
			$(this).addClass("open");
		}

	});

	$('.burger-menu').on('click' , function(){
		if ( $('.toogle-menu').hasClass("menu-open") ) {
			$(this).removeClass("open");
			$('.toogle-menu').removeClass('menu-open');
		} else {
			$('.toogle-menu').addClass('menu-open');
			$(this).addClass("open");
		}
	});

	$('input , textarea').focus(function () {
		$(this).next('span').find('.fa').css('color' ,"#00aeef");
	}).focusout(function () {
		$(this).next('span').find('.fa').css('color' ,"#dadcde");
	});

	/* Applicant review layer */

    $('.wrapper-question-item').on('click' , function (e) {
        e.preventDefault();
               var $this = $(this);

                 var liencurrent = window.location.href;
             
                if (liencurrent.match(/en/)) {
                  $('.label-select').html('watch');
                  $this.find('.label-select').html('now playing');
                 
                 }else{
                     
                      $('.label-select').html('voir');
                     $this.find('.label-select').html('Lecture en cours');
                 }
             
            
            $('.label-select').removeClass('now-playing').addClass('watch-label');
            $this.find('.label-select').addClass('now-playing').removeClass('watch-label');

            $('.wrapper-question-item').removeClass('active');
            $this.addClass('active');
            var videoClick = $this.data('idapplicant');
            $('.application-video').removeClass('active');
            $('#application-video-player-'+videoClick).addClass('active');

            $("video").each(function(){
             $(this).get(0).pause();
            });
    });

	$('.wrapper-question-item').on('click' , function (e) {
		e.preventDefault();
			   var $this = $(this);

			     var liencurrent = window.location.href;
			 
				if (liencurrent.match(/en/)) {
				  $('.label-select').html('watch');
				  $this.find('.label-select').html('now playing');
				 
				 }else{
				 	
				 	 $('.label-select').html('voir');
				     $this.find('.label-select').html('Lecture en cours');
				 }
			 
			
			$('.label-select').removeClass('now-playing').addClass('watch-label');
			$this.find('.label-select').addClass('now-playing').removeClass('watch-label');

			$('.wrapper-question-item').removeClass('active');
		    $this.addClass('active');
		    var videoClick = $this.data('idapplicant');
		    $('.application-video').removeClass('active');
			$('#application-video-player-'+videoClick).addClass('active');

			$("video").each(function(){
			 $(this).get(0).pause();
			});
	});

	$('.modal-applicant').on('hidden.bs.modal', function () {
		var idModal = $(this).attr('id');
		$("#"+idModal+" video").each(function(){
			 $(this).get(0).pause();
			});
	})

	/* Open Close job */
	if ($(".job-open-close")[0]){
	$('.job-open-close input[type=radio]').change(function() {
    var slug = $(this).closest('tr').data('job-slug');
    var row_id =  $(this).closest('tr').attr('id');

    $.ajax({
        url: Routing.generate('job_change_status', { slug: slug}),
        type: 'POST',
        data: {  slug: slug },
        success: function(rs) {

            if ( $('#'+row_id).hasClass('red-ligne') )
            {
                $('#'+row_id).removeClass('red-ligne');
            }else
            {
                 $('#'+row_id).addClass('red-ligne');
            }

        }
        });
});
}
	/* hireAction */
	$('.applicant-hired').on('change', 'input[name*="hired"][type=radio]', function(e) {
		e.preventDefault();

		var changed = $(this).attr('name'),
			applicantId = $(this).closest('tr').data('applicant'),
			jobSlug = $(this).closest('tr').data('job-slug');

		$.post(
			Routing.generate('job_applicant_hired', {slug: jobSlug, applicantId: applicantId})
		);
	});

	/* Invite Action */
	 $("#applicants-list").on("click", "button.send-invitation", function(e){
		 e.preventDefault();
		 if ( $(this).not('.disabled-btn') ){

		var _this = this,
			applicantId = $(this).closest('tr').data('applicant'),
			jobSlug = $(this).closest('tr').data('job-slug');

		$.post(
			Routing.generate('job_applicant_invite', {slug: jobSlug, applicantId: applicantId}),
			function(data) {
				if (data.success) {
					$(_this)
						.removeClass('send-invitation')
						.addClass('disabled-btn')
						.text('Sent')
						.off('click')
					;
				}
			}
		);
		 }
	 });
	

	/* Scroll bar Style */
	if ( $('.tab-content')[0]) {
		$('.tab-content').mCustomScrollbar({
			theme:"haystack",
			autoHideScrollbar: false,
			scrollButtons:{enable:false}
		});
	}

	if ( $('.btn-instruction')[0] ) {
		$('.btn-instruction').hover(function(){
			var id = $(this).data('tooltip');
			$('#tooltip-'+id).css('display', 'block');
		},
		function(){
			var id = $(this).data('tooltip');
			$('#tooltip-'+id).css('display', 'none');
		}
		);
	}

	
	$('.container-preview-questions').on('mouseenter', '.btn-instruction-test', function() {
		 $(this).popover('show');
	});
	
	$('#preview-question').on('click' , function(e){
		e.preventDefault();
		var slug = $(this).data('slug');
	$.ajax({
		url :  Routing.generate('job_preview_question', { slug: slug}),
		type: 'POST',
		success: function(rs){
			$('.container-preview-questions').show();
			$('.container-preview-questions').html(rs.questions);
			$('.modal-header').hide();
			$('.create-job-footer-modal .save-next').hide();
			$('#quit-preview-question').show();
			$('.container-modal').hide();
			$('#preview-question').hide();
		}
	});
	});
	$('#quit-preview-question').on('click', function(){
		$('.container-preview-questions').hide();
		$('.create-job-footer-modal .save-next').show();
		$('#quit-preview-question').hide();
		$('.modal-header').show();
		$('.container-modal').show();
		$('#preview-question').show();
	});

/*
	$('.advance-applicant input[type=radio]').change( function() {
		var id = $(this).closest('.modal-applicant').data('applicant-id');
		alert(id);
		$('#applicant_update_advanced .fa').remove();
		$(this).prev().append('<i class="fa fa-check" aria-hidden="true"></i>');
	});
*/
$('.reffered input[type=radio]').change( function(e) {
	e.preventDefault();
	if ( $(this).val() == 'yes') {
		$('.info-referred-person').show();
	}else{
		$('.info-referred-person').hide();
	}
});

$('.advance-applicant label').click(function(e) {
		e.preventDefault();
		var id = $(this).closest('.advance-applicant').attr('id');
	   	
		if( $(this).prev('input:radio').is(':checked')){
			$(this).prev('input:radio').prop( "checked", false );
			$('#'+ id +' .fa').remove();
			return false;
		}
	   $(this).prev('input:radio').prop( "checked", true );
	   $('#'+ id +' .fa').remove();
	   $(this).append('<i class="fa fa-check" aria-hidden="true"></i>')
});

	$('form[name="applicant_review"]').submit(function(e){
		e.preventDefault();
		var formSerialize = $(this).serialize();
		var id = $(this).attr('id');
		$.ajax({
        url: Routing.generate('applicant_video', { id: id}),
		data : formSerialize,
        type: 'POST',
		success: function(rs) {
                     
                var liencurrent = window.location.href;
             
                if (liencurrent.match(/en/)) {
                    swal({
                    title: 'Good job!',
                    text: 'Update with successfully',
                    type: 'success',
                    html: true
                    });
                 
                 }else{
                     
                    swal({
                    title: 'Bon travail!',
                    text: 'Mettre à jour avec succès',
                    type: 'success',
                    html: true
                    }); 
                 }

			
		}
		});
	});

	$('form[name="applicant_update"]').submit(function(e){
		e.preventDefault();
		var formSerialize = $(this).serialize();
		var id = $(this).attr('id');
		var job = 'adelphatech-front-developer';
		var slug = $('.modal-applicant').data('slug');
		$('body').addClass('hay-open-modal');
		$.ajax({
        url: Routing.generate('job_applicant', { applicantId: id, slug: slug}),
		data : formSerialize,
        type: 'POST',
		success: function(rs) {
		/* Update  list applicants */	
			$.ajax({
			url: Routing.generate('job_applicant_datas', { slug: slug}),
			type: 'POST',
			success: function(rs) {
				$('#applicants-list').html(rs);
				$('.modal-applicant').modal('hide');
				var newId = $('#applicant-'+id).parent('form').nextAll('form.form-update').first().attr('id');
				$('#applicant-'+newId).modal('show');
			}
			});
		
		/* end update  list applicants */		

		}
		});
	});

	$('#begin-interview-link').on('click' , function(e){
      e.preventDefault();
      var token = $(this).data('token');
	 $('#global-interview-video').html('<div class="loader-video"></div>');
      $.ajax({
       url : Routing.generate('applicant_interview_question', { token: token}),
       type: 'POST',
       success: function(rs) {
		 if (rs.notNextVideo) {
            $('#global-interview-video').html(rs.thankyouContent);

        } else{
            $('#global-interview-video').html(rs.question);
			$('#applicant_video_writtenAnswer').val('');
			$('.container-video .vjs-device-button').trigger( "click" );
			$('.container-video .vjs-icon-record-start').trigger( "click" );
        }

       },
       error: function(error) {
        console.log(error);
       }
     });

});
	$('.play-pause').on('click', function(e){
		e.preventDefault();
		var video = $(this).data('video');
		$('video#'+video)[0].play();
	});


	 $('#btn-full-description').on('click' , function(e)
	 {
		 e.preventDefault();
		 $(this).toggleClass("active");
		 $('.job-name p').toggleClass("hide");
	 });
	 $('.btn-full-description-video').on('click' , function(e)
	 {
		 e.preventDefault();
		 var id = $(this).data('description');
		 $(this).toggleClass("active");
		 $('#'+ id + ' p').toggleClass("hide");
	 });
	 
	 
	  $('input[type=radio][name=pp]').change(function() {
		console.log($(this));
		var id = $(this).data('checked');
		$('.price-table-content').removeClass('price-selected');
		$('#'+id).addClass('price-selected');
	  });

	 if ( $('form[name="applicant"]')[0] ) {
		   $('form[name="applicant"]').validate();
	  };
	 
	  $('#configure-job-step-1').on('hidden.bs.modal', function () {
		  $('body').removeClass('stop-scrolling');
	  });

	  //var prototype = $('#question_video_questionsVideo').data('prototype');
	  //$('#add-new-question-video .add-new-question-video').html(prototype);

	    $('.save-next-question-video').on('click', function(e){
		  e.preventDefault();
		   var id = $(this).attr('id');
		   var data = $('form[name="question_video"]').serialize();
		    $.ajax({
				type: 'POST',
				url: Routing.generate('update_question_video', { id: id}),
				data: data,
				success: function(rs){
					console.log(rs);
				}
		});	
	  });	 
})(jQuery);

