$(function() {
    $(document)
        .ajaxSuccess(function (event, jqXHR, ajaxOptions, data) {
            if ($.isArray(data.messages)) {
                ul = $('<ul/>');
                $.each(data.messages, function(index, message) {
                    ul.append($('<li/>').text(message));
                });

                var message = ul.wrap('<ul/>').parent().html();
            } else {
                var message = data.messages;
            }
            if (typeof swal !== 'undefined' && $.isFunction(swal)) {
            swal({
                title: data.title,
                text: message,
                type: data.success ? 'success' : 'error',
                html: true
            });
            }
        })
        .ajaxError(function (event, jqXHR) {
            if (403 === jqXHR.status) {
                window.location.reload();
            }
        })
    ;

    var initSortable = function() {
        if (typeof $.fn.sortable !== 'undefined') {
            $('#my-questions').sortable({
                placeholder: 'ui-state-highlight',
                items: '.question',
                activate: function () {

                },
                update: function() {
                    var order = $(this).sortable('toArray' , {
                        attribute: "id"
                    });
                    console.log(order);
                    $.each( order, function( key, value ) {
                      $('#'+ value + ' .nb').html(key+1);
                    })
                    var jobSlug = $('#my-questions').data('job');
                    $.ajax({
                        type: "POST",
                        url: Routing.generate('job_question_order', {'slug':jobSlug}),
                        data: {order:order},

                    }).done(function(rs) {
                      // $('#tab-question').html(rs.newList);
                      // $('#my-questions').sortable();
                    });
                },
                stop: function (event , ui) {

                }
            });

            $( "#my-questions" ).disableSelection();
        }
    }

    initSortable();

    var initCollection = function() {
        if ($('.collection').length) {
            $('.collection').collection({
                min: 2,
                max: 100,
                init_with_n_elements: 2,
                allow_add: true,
                allow_remove: true,
                allow_up: false,
                allow_down: false,
                add_at_the_end: false,
                add: 'collection-add',
                delete: 'collection-remove',
                
            });
        }
    }

    initCollection();
    //*** Required Documents ****************************************************
    $('.cnt-candidate').on('change', 'input[type="checkbox"][value="other"]', function(e) {
        var textField = $('.container-new-document').find('input[type="text"]');
        if ($(e.target).is(':checked')) {
            $(textField).prop('disabled', false);
        } else {
            $(textField).prop('disabled', true).val('');
        }
    });

    $('form[name="documents"]').on('submit', function(e) {
        e.preventDefault();

        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            if ('closeModal' in e && e.closeModal) {
                $('#configure-job-step-1').modal('hide');
            }
        });
    });

    //*** Questions *************************************************************
    var initQuestionDroppable = function(questionsRemaining) {
        var questionsRemaining = questionsRemaining || true;
                $('.btn-draggable').on('click', function(e){
                e.preventDefault();
                var thisVal = $(this).val();
                $('.tab-content-first-question').hide();
                var typeQuestion = $(this).data('type-question');

                $('#my-questions').hide();
                $('.nav-tabs a[href="#tab-configure"]').tab('show');

                $('#tab-configure').removeClass('multiple-choice check-boxes dropdown paragraph short-answer');
                $('.configure-question').css('display' , 'none');
                $('button.btn-draggable').prop('disabled', true);
                $('.create-job-footer-modal .btn').prop('disabled', true);
                $('.help-text').addClass('no-active');
                $('.'+ thisVal +'-widget').html('<div class="loader">....</div>');
                
                $.get(
                    Routing.generate(
                        'job_question_add',
                        {
                            questionType: thisVal,
                            'slug': $('#job-slug').val()
                        }
                    ),
                    function(data) {

                    }
                    )

                 
                    
                .done(function(data, xhr, options){ 
                setTimeout(function(){
               $('.'+ thisVal +'-widget').html(data); 
                        if (thisVal== 'checkbox'){
                            $('.dropdown-widget #add_question_answers').attr('id', 'add_question_answer_'+thisVal);
                            $('.radio-widget #add_question_answers').attr('id', 'add_question_answer_'+thisVal);
                            $('.checkbox-widget #add_question_question').focus();
                        }else if (thisVal== 'dropdown'){
                            $('.checkbox-widget #add_question_answers').attr('id', 'add_question_answer_'+thisVal);
                            $('.radio-widget #add_question_answers').attr('id', 'add_question_answer_'+thisVal);
                            $('.dropdown-widget #add_question_question').focus();
                            
                        }else if ( thisVal == 'radio' ){
                         $('.dropdown-widget #add_question_answers').attr('id', 'add_question_answer_'+thisVal);
                         $('.checkbox-widget #add_question_answers').attr('id', 'add_question_answer_'+thisVal);
                         $('.radio-widget #add_question_question').focus();
                         
                        }else if (thisVal == 'paragraph'){
                             $('.paragraph-widget #add_question_question').focus();
                        }else if ( thisVal == 'short_answer'){
                             $('.short_answer-widget #add_question_question').focus();

                        }
                         initCollection();
                }, 5000);
                   
                  
                         

                });

                $('#tab-configure').addClass(typeQuestion);

        });
    };

    initQuestionDroppable();
    $(document).on('haystack:questions:sortable', function(e) {
        initSortable();
    });
    $(document).on('haystack:questions:buttons:droppable', function(e, questionsRemaining) {
        initQuestionDroppable(questionsRemaining);
    });

    $(document).on('haystack:questions:update:list_view', function(e, questionsListView) {
        $('#tab-question').html(questionsListView);
    });

    $(document).on('haystack:questions:update:remaining', function(e, questionsRemaining) {
        $('.container-questions').find('.questions-remaining .number').text(questionsRemaining);

        if (questionsRemaining == 0) {
            $('.types-questions').find('button.btn-draggable').prop('disabled', true);
        } else {
            $('.types-questions').find('button.btn-draggable').prop('disabled', false);
        }
    });

    $(document).on('submit', 'form[name="add_question"]', function(e) {
        e.preventDefault();

        var _this = this;
           //console.log(_this);
           //var fieldsVide = $(this).closest(_this).find("input[type=text]").val();
           //console.log(fieldsVide);
        $.post(
            $(this).attr('action'),
            $(this).serialize(), function(data) {

                if (data.success) {
                    $(document).trigger('haystack:questions:update:list_view', [data.questions_list_view]);
                    $(document).trigger('haystack:questions:update:remaining', [data.questions_remaining]);
                    $(document).trigger('haystack:questions:buttons:droppable', [data.questions_remaining]);
                    $(document).trigger('haystack:questions:sortable');
                    $(e.target).find('.btn-cancel').trigger('click');
                    $('#my-questions').show();
                }
            }
        )
    });

    $(document).on('click', '.btn-edit-question', function(e) {
        e.preventDefault();
        var _this = this
            typeQuestion = $(_this).data('question-type')
        ;

        var cssClasses = {
            'radio': 'multiple-choice',
            'checkbox': 'check-boxes',
            'dropdown': 'dropdown',
            'short_answer': 'short-answer',
            'paragraph': 'paragraph'
        };

       
        $.get($(this).attr('href'), 
        function(data) {
            //console.log(data);
            $('.types-questions').find('button.btn-draggable').prop('disabled', true);
            $('.'+ typeQuestion +'-widget form').remove();
            $('.'+ typeQuestion +'-widget').html(data);
            $('#my-questions').hide();
            $('#tab-configure').addClass(cssClasses[typeQuestion]);
            $('.nav-tabs a[href="#tab-configure"]').tab('show');
            $('button#preview-question').attr('disabled', 'disabled');
            initCollection();
        });
    });
    //*** Questions *************************************************************//
    /* save configure QUESTIONS */
    $('.btns-question button.btn-save').on('click' , function(e){
       e.preventDefault();
       var question = $(this).data('question');
       $.ajax({
           /* Options ajax */
       });
    });

     /* delete configure QUESTIONS*/
    $(document).on('click', '.btn-delete-question', function(e) {
        e.preventDefault();

        var questionContainer = $(this).parents('.question-container');

        if (confirm('Are you sure you want to delete this question?')) {
            $.get($(this).attr('href'), function(data) {
                if (data.success) {
                    $(document).trigger('haystack:questions:update:list_view', [data.questions_list_view]);
                    $(document).trigger('haystack:questions:update:remaining', [data.questions_remaining]);
                    $(document).trigger('haystack:questions:buttons:droppable', [data.questions_remaining]);
                    $(document).trigger('haystack:questions:sortable');
                    switchToQuestionTab();
                }
            });
        }

    });

    var switchToQuestionTab = function() {
        $('.tab-pane').removeClass('multiple-choice check-boxes dropdown short-answer paragraph');
        $('.nav-tabs a[href="#tab-question"]').tab('show');
        $('#my-questions').show();

        $('.create-job-footer-modal .btn').prop('disabled', false);
        $('.help-text').removeClass('no-active');
        $('#tab-configure').find('form[name="add_question"]').hide();
    };

    /* cancel configure QUESTIONS */
    $('.configure-question').on('click', 'button.btn-cancel', function (e) {
        e.preventDefault();

        $('.tab-content-first-question').show();
        switchToQuestionTab();

        var questionsRemaining = parseInt($('.container-questions').find('.questions-remaining .number').text());
        $(document).trigger('haystack:questions:update:remaining', [questionsRemaining]);
    });

    /* Preview Question*/
    $('button.preview-question').on('click', function (e) {
        e.preventDefault();
        $('.nav-tabs a[href="#tab-question"]').tab('show');
    });

    if (typeof Cleave !== 'undefined') {
        var cleaveDate = new Cleave('form input[id*=creditCard_expirationDate]', {
            date: true,
            datePattern: ['m', 'y']
        });

        var cleaveCreditCard = new Cleave('form input[id*=creditCard_number]', {
            creditCard: true
        });
    }

    $(document).on('click', '.create-job-footer-modal .save-next', function(e) {
         var modalType = $(this).data('modaltype');
         var questionsRemaining = $('.questions-remaining .number').html();
        $('form[name="documents"]').trigger({
            type: 'submit',
            closeModal: true
        });
            
        
        if(questionsRemaining == 0 &&  modalType == "create-questions"){
           
            $('#box-create-questions').removeClass('active valid').addClass('disabled');
            $('#box-create-questions .fa').removeClass('fa-search').addClass('fa-check')
            $('#box-create-video').removeClass('disabled').addClass('active');
            $('.btn-edit-box-create-questions').addClass('show');
        }else if(modalType == "create-questions" && questionsRemaining != 0) {
            $('#box-create-questions').removeClass('disabled').addClass('active valid');
            $('#box-create-questions .fa').removeClass('fa-check').addClass('fa-search')
            $('#box-create-video').removeClass('active').addClass('disabled');
            $('.btn-edit-box-create-questions').removeClass('show');
            
        }
        if (questionsRemaining == 0 &&  modalType == "create-video") {
            console.log('VIDEOOOOOO 333');
            $('#box-create-video').removeClass('active').addClass('disabled');
            $('.btn-edit-box-create-video').addClass('show');            
        }else if(modalType == "create-video" && questionsRemaining != 0){
            $('#box-create-video').removeClass('disabled').addClass('active');
            $('.btn-edit-box-create-video').removeClass('show'); 
            
        }
    });
});
