<?php

namespace OpauthBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OpauthBundle extends Bundle
{
    public function getParent()
    {
        return 'DCSOpauthBundle';
    }
}
