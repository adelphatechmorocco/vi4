<?php

namespace OpauthBundle;

class Profile
{
    private $fullname;

    private $firstname;

    private $lastname;

    private $email;

    private $headline;

    private $description;

    private $location;

    public function __construct(array $data = null)
    {
        if (isset($data['auth']) && isset($data['auth']['provider'])) {
            switch ($data['auth']['provider']) {
                case 'facebook':
                    $this
                        ->setFullname($data['auth']['info']['name'])
                        ->setFirstname($data['auth']['info']['first_name'])
                        ->setLastname($data['auth']['info']['last_name'])
                        ->setEmail($data['auth']['info']['email'])
                        ->setLocation($data['auth']['info']['location'])
                    ;
                break;

                case 'twitter':
                    $this
                        ->setFullname($data['auth']['info']['name'])
                        ->setFirstname($data['auth']['info']['name'])
                        ->setDescription($data['auth']['info']['description'])
                        ->setLocation($data['auth']['info']['location'])
                    ;
                break;

                case 'google':
                    $this
                        ->setFullname($data['auth']['info']['name'])
                        ->setFirstname($data['auth']['info']['first_name'])
                        ->setLastname($data['auth']['info']['last_name'])
                        ->setEmail($data['auth']['info']['email'])
                    ;
                break;

                case 'linkedin':
                    $this
                        ->setFullname(
                            isset($data['auth']['raw']['formatted-name']) ? $data['auth']['raw']['formatted-name'] : null
                        )
                        ->setFirstname(
                            isset($data['auth']['raw']['first-name']) ? $data['auth']['raw']['first-name'] : null
                        )
                        ->setLastname(
                            isset($data['auth']['raw']['last-name']) ? $data['auth']['raw']['last-name'] : null
                        )
                        ->setEmail($data['auth']['info']['email'])
                        ->setHeadline(
                            isset($data['auth']['raw']['headline']) ? $data['auth']['raw']['headline'] : null
                        )
                        ->setDescription(
                            isset($data['auth']['raw']['summary']) ? $data['auth']['raw']['summary'] : null
                        )
                        ->setLocation(
                            (isset($data['auth']['raw']['location']) && isset($data['auth']['raw']['location']['name'])) ? $data['auth']['raw']['location']['name'] : null
                        )
                    ;
                break;
            }
        }
    }

    public function setFullname($fullname = null)
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getFullname()
    {
        return $this->fullname;
    }

    public function setFirstname($firstname = null)
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setLastname($lastname = null)
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getHeadline()
    {
        return $this->headline;
    }

    public function setHeadline($headline = null)
    {
        $this->headline = $headline;

        return $this;
    }

    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setLocation($location = null)
    {
        $this->location = $location;

        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }
}
