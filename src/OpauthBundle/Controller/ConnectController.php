<?php

namespace OpauthBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ConnectController extends Controller
{
    /**
     * Call the provider for the authentication.
     */
    public function loginAction(Request $request, $strategy)
    {
        if ($request->query->has('next')) {
            $request->getSession()->set('next_url', $request->query->get('next'));
        }

        $config = $this->get('dcs_opauth.strategy_parser')->get($strategy);
        $opauth = new \Opauth($config);

        die('oooo');
    }
}
