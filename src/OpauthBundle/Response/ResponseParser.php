<?php

namespace OpauthBundle\Response;

use Symfony\Component\HttpFoundation\Session\Session;
use DCS\OpauthBundle\Exception\UnsupportedCallbackException;
use DCS\OpauthBundle\Response\ResponseParserInterface;

class ResponseParser implements ResponseParserInterface
{
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * Parse response of the Opauth object
     *
     * @param \Opauth $opauth
     * @return array
     * @throws \DCS\OpauthBundle\Exception\UnsupportedCallbackException
     */
    public function parse(\Opauth $opauth)
    {
        $callbackTransport = $opauth->env['callback_transport'];

        switch($callbackTransport) {
            case 'session':
                //dump($this->session->all());die;
                if (!session_id()) {
                    session_start();
                }

                $response = $_SESSION['opauth'];
                unset($_SESSION['opauth']);
                break;
            case 'post':
                $response = unserialize(base64_decode($_POST['opauth']));
                break;
            case 'get':
                $response = unserialize(base64_decode($_GET['opauth']));
                break;
            default:
                throw new UnsupportedCallbackException(sprintf('Callback transport "%s" is not supported. Are only supported: session, post, get', $callbackTransport));
                break;
        }

        return $response;
    }
}
