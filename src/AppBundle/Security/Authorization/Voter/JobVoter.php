<?php

namespace AppBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Entity\Company;

class JobVoter extends AbstractVoter
{
    const VIEW = 'view';
    const EDIT = 'edit';

    protected function getSupportedAttributes()
    {
        return [
            self::VIEW,
            self::EDIT,
        ];
    }

    protected function getSupportedClasses()
    {
        return ['AppBundle\Entity\Job'];
    }

    protected function isGranted($attribute, $job, $user = null)
    {
        if (!$user instanceof UserInterface) {
            return false;
        }

        if (in_array($attribute, $this->getSupportedAttributes())) {
            if ($user->getCompany()->getId() === $job->getOwner()->getId()) {
                return true;
            }
        }

        return false;
    }
}
