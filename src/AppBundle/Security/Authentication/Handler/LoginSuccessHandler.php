<?php

namespace AppBundle\Security\Authentication\Handler;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use AppBundle\Entity\Employer;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private $container;
    private $session;
    private $router;
    private $tokenStorage;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->session = $container->get('session');
        $this->router = $container->get('router');

        if ($this->container->has('security.token_storage')) {
            $this->tokenStorage = $this->container->get('security.token_storage');
        } else {
            $this->tokenStorage = $this->container->get('security.context');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();

        $url = $this->router->generate('jobs_list');

        if (!$user->isRegistrationDone()) {
            $stepsRoutes = [
                Employer::STEP_CREATE_EMAIL_PASSWORD => 'fos_user_registration_register',
                Employer::STEP_MODEL_PRICING         => 'employer_register_model_pricing',
                Employer::STEP_ACCOUNT_BILLING       => 'employer_register_account_billing',
            ];

            while (key($stepsRoutes) !== $user->getStep()) {
                next($stepsRoutes);
            }

            $url = $this->router->generate(next($stepsRoutes));

        } else {
            $defaultTargetPath = sprintf('_security.%s.target_path', $this->tokenStorage->getToken()->getProviderKey());
            if ($this->session->has($defaultTargetPath)) {
                $url = $this->session->get($defaultTargetPath);
            }
        }

        return new RedirectResponse($url);
    }
}
