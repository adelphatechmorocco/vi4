<?php

namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use AppBundle\Entity\Employer;
use AppBundle\Workflow\Model\EmployerModel;

class UserListener implements EventSubscriberInterface
{
    private $container;

    private $router;

    private $em;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->router = $container->get('router');
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => 'onKernelRequest',
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialized',
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccessed',
        );
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $excludeRoutes = [
            'employer_register_model_pricing',
            'employer_register_account_billing',
        ];

        if (in_array($request->get('_route'), $excludeRoutes)) {
            return;
        }

        $token = $this->container->get('security.token_storage')->getToken();
        if ($token && $this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            if (!$user->isRegistrationDone()) {
                $stepsRoutes = [
                    Employer::STEP_CREATE_EMAIL_PASSWORD => 'fos_user_registration_register',
                    Employer::STEP_MODEL_PRICING         => 'employer_register_model_pricing',
                    Employer::STEP_ACCOUNT_BILLING       => 'employer_register_account_billing',
                ];

                while (key($stepsRoutes) !== $user->getStep()) {
                    next($stepsRoutes);
                }

                $event->setResponse(new RedirectResponse(
                    $this->router->generate(next($stepsRoutes))
                ));
            }
        }
    }

    public function onRegistrationInitialized(GetResponseUserEvent $event)
    {
    }

    public function onRegistrationSuccessed(FormEvent $event)
    {
        $event->setResponse(new RedirectResponse(
            $this->router->generate('employer_register_model_pricing')
        ));
    }
}
