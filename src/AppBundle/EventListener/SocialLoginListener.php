<?php

namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use DCS\OpauthBundle\DCSOpauthEvents;
use DCS\OpauthBundle\Event\OpauthResponseEvent;
use OpauthBundle\Profile;

class SocialLoginListener implements EventSubscriberInterface
{
    private $container;

    private $request;

    private $router;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->request = $container->get('request');
        $this->router = $container->get('router');
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            DCSOpauthEvents::AFTER_PARSE_RESPONSE => 'onAfterParseResponse',
        );
    }

    public function onAfterParseResponse(OpauthResponseEvent $event)
    {
        $event->setAuthenticate(false);

        $opauth = $event->getOpauth();
        $responseData = $event->getResponseData();

        $data = $event->getResponseData();
        $this->request->getSession()->set('user_social_data', $data);

        $profile = new Profile($data);
        $this->request->getSession()->set('user_profile', $profile);

        $event->setResponse(
            new RedirectResponse($this->request->getSession()->get('next_url'))
        );
    }
}
