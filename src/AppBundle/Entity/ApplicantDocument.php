<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use AppBundle\Utils\Utils;

/**
 * @ORM\Table(name="`hs_applicant_document")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicantRepository")
 * @Vich\Uploadable
 */
class ApplicantDocument
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Applicant")
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
	private $applicant;

    /**
     * @ORM\ManyToOne(targetEntity="JobDocument")
     * @ORM\JoinColumn(name="job_document_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $jobDocument;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="bigint")
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="document", type="string", length=255, nullable=false)
     */
    private $document;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="applicant_document", fileNameProperty="document")
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {
     *      "application/pdf",
     *      "application/x-pdf",
     *      "application/msword",
     *      "application/xml-dtd",
     *      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
     *      "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
     *      "application/vnd.sun.xml.writer",
     *      "application/vnd.sun.xml.writer.global",
     *      "application/vnd.oasis.opendocument.text"
     *     }
     * )
     */
    private $documentFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    public function __toString()
    {
        return $this->document;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return ApplicantDocument
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getHumanSize()
    {
        return Utils::humanFileSize($this->size);
    }

    /**
     * Set document
     *
     * @param string $document
     * @return ApplicantDocument
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ApplicantDocument
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set applicant
     *
     * @param \AppBundle\Entity\Applicant $applicant
     *
     * @return ApplicantDocument
     */
    public function setApplicant(\AppBundle\Entity\Applicant $applicant)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return \AppBundle\Entity\Applicant
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    public function getDocumentFile()
    {
        return $this->documentFile;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setDocumentFile(File $documentFile = null)
    {
        $this->documentFile = $documentFile;

        if ($documentFile && $documentFile->isFile()) {
            $this->size = $documentFile->getSize();
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Set jobDocument
     *
     * @param \AppBundle\Entity\JobDocument $jobDocument
     *
     * @return ApplicantDocument
     */
    public function setJobDocument(\AppBundle\Entity\JobDocument $jobDocument)
    {
        $this->jobDocument = $jobDocument;

        return $this;
    }

    /**
     * Get jobDocument
     *
     * @return \AppBundle\Entity\JobDocument
     */
    public function getJobDocument()
    {
        return $this->jobDocument;
    }
}
