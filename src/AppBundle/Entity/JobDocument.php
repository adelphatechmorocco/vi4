<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="`hs_job_document`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JobDocumentRepository")
 */
class JobDocument
{
    const DOCUMENT_TYPE_RESUME       = 'resume';
    const DOCUMENT_TYPE_COVER_LETTER = 'cover_letter';
    const DOCUMENT_TYPE_REFERENCE    = 'reference';
    const DOCUMENT_TYPE_OTHER        = 'other';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $job;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="document_type", type="string", length=255)
     */
    private $documentType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    public static function getDocumentTypeChoices()
    {
        return [
            self::DOCUMENT_TYPE_RESUME,
            self::DOCUMENT_TYPE_COVER_LETTER,
            self::DOCUMENT_TYPE_REFERENCE,
            self::DOCUMENT_TYPE_OTHER,
        ];
    }

    public static function getDocumentTypeChoicesLabels()
    {
        return [
            self::DOCUMENT_TYPE_RESUME       => 'form.documents.document_type.resume',
            self::DOCUMENT_TYPE_COVER_LETTER => 'form.documents.document_type.cover_letter',
            self::DOCUMENT_TYPE_REFERENCE    => 'form.documents.document_type.reference',
            self::DOCUMENT_TYPE_OTHER        => 'form.documents.document_type.other',
        ];
    }

    public function __toString()
    {
        return $this->name?: $this->documentType;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return JobDocument
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set documentType
     *
     * @param string $documentType
     * @return JobDocument
     */
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;

        return $this;
    }

    /**
     * Get documentType
     *
     * @return string
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    public function getDocumentTypeLabel()
    {
        if (self::DOCUMENT_TYPE_OTHER === $this->documentType) {
            return $this->name;
        }

        return self::getDocumentTypeChoicesLabels()[$this->documentType];
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return JobDocument
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set job
     *
     * @param \AppBundle\Entity\Job $job
     * @return JobDocument
     */
    public function setJob(\AppBundle\Entity\Job $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \AppBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }
}
