<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="`hs_answer`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnswerRepository")
 */
class Answer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="Applicant", inversedBy="answers")
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $applicant;

    /**
     * @var string
     *
     * @ORM\Column(name="answers_metadata", type="array")
     */
    private $answersMetadata;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function __toString()
    {
        return $this->answer;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Answer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAtsetQuestion
     *
     * @param \DateTime $updatedAt
     * @return Answer
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Question $question
     * @return Answer
     */
    public function setQuestion(\AppBundle\Entity\Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set applicant
     *
     * @param \AppBundle\Entity\Applicant $applicant
     * @return Answer
     */
    public function setApplicant(\AppBundle\Entity\Applicant $applicant)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return \AppBundle\Entity\Applicant
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    /**
     * Set answersMetadata
     *
     * @param array $answersMetadata
     *
     * @return Answer
     */
    public function setAnswersMetadata($answersMetadata)
    {
        $this->answersMetadata = $answersMetadata;

        return $this;
    }

    /**
     * Get answersMetadata
     *
     * @return array
     */
    public function getAnswersMetadata()
    {
        if (is_array($this->answersMetadata)) {

            return $this->answersMetadata;
        }

        return $this->answersMetadata;
    }

    /**
     * Get answersMetadata
     *
     * @return array
     */
    public function getAnswersMetadataObjects()
    {
        $questionAnswers = $this->getQuestion()->getAnswers();

        if (is_array($this->answersMetadata)) {
            $answers = [];
            foreach ($this->answersMetadata as $answerIndex) {
                if (isset($questionAnswers[$answerIndex])) {
                    $answers[] = $questionAnswers[$answerIndex];
                }
            }

            return $answers;
        }

        if (is_numeric($this->answersMetadata)) {
            if (isset($questionAnswers[$this->answersMetadata])) {
                return $questionAnswers[$this->answersMetadata];
            }

            return;
        }

        return $this->answersMetadata;
    }
}
