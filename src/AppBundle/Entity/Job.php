<?php

namespace AppBundle\Entity;


use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use PUGX\Shortid\Shortid;
use AppBundle\Entity\Applicant;

/**
 * @ORM\Table(name="`hs_job`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JobRepository")
 * @Vich\Uploadable
 */
class Job
{
    const QUESTIONS_LIMIT = 5;

    const STATUS_JOB_OPEN  = 'open';
    const STATUS_JOB_CLOSE = 'close';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="jobs")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="JobDocument", mappedBy="job", cascade={"all"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="job", cascade={"all"})
     * @ORM\OrderBy({"order" = "ASC"})
     */
    private $questions;

    /**
     * @ORM\OneToMany(targetEntity="QuestionVideo", mappedBy="job",  cascade={"all"})
     * @Assert\NotBlank(groups={"JobSecondStep"})
     * @ORM\OrderBy({"order" = "ASC"})
     */
    private $questionsVideo;

    /**
     * @ORM\OneToMany(targetEntity="JobLink", mappedBy="job", cascade={"all"})
     * @Assert\NotBlank(groups={"JobLastStep"})
     */
    private $links;

    /**
     * @ORM\OneToMany(targetEntity="Applicant", mappedBy="job", cascade={"all"})
     */
    private $applicants;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank(groups={"JobFirstStep"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=false)
     * @Gedmo\Slug(fields={"companyName", "title"})
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=500)
     * @Assert\NotBlank(groups={"JobFirstStep"})
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     * @Assert\NotBlank(groups={"JobFirstStep"})
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=10)
     * @Assert\NotBlank(groups={"JobFirstStep"})
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank(groups={"JobFirstStep"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255)
     * @Assert\NotBlank(groups={"JobFirstStep"})
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="company_logo", type="string", length=255, nullable=true)
     */
    private $companyLogo;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="job_company_logo", fileNameProperty="companyLogo")
     * @Assert\File(
     *     maxSize = "2M",
     *     mimeTypes = {
     *         "image/jpg",
     *         "image/jpeg",
     *         "image/pjpeg",
     *         "image/png",
     *     }
     * )
     */
    private $companyLogoFile;

    /**
     * @ORM\ManyToOne(targetEntity="BackgroundTexture")
     * @ORM\JoinColumn(name="background_texture_id", referencedColumnName="id", nullable=true)
     * @Assert\Valid
     */
    private $backgroundTexture;

    /**
     * @var string
     *
     * @ORM\Column(name="background_color", type="string", length=255, nullable=true)
     */
    private $backgroundColor;

    /**
     * @var string
     *
     * @ORM\Column(name="company_logo_preview", type="string", length=255, nullable=true)
     * Assert\NotBlank(groups={"JobThirdStep"})
     */
    private $companyLogoPreview;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="job_company_logo_preview", fileNameProperty="companyLogoPreview")
     * @Assert\File(
     *     maxSize = "2M",
     *     mimeTypes = {
     *         "image/jpg",
     *         "image/jpeg",
     *         "image/pjpeg",
     *         "image/png",
     *     }
     * )
     */
    private $companyLogoFilePreview;

    /**
     * @ORM\ManyToOne(targetEntity="BackgroundTexture")
     * @ORM\JoinColumn(name="background_texture_preview_id", referencedColumnName="id", nullable=true)
     * @Assert\Valid
     */
    private $backgroundTexturePreview;

    /**
     * @var string
     *
     * @ORM\Column(name="background_color_preview", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"JobThirdStep"})
     */
    private $backgroundColorPreview = '#ffffff';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", columnDefinition="ENUM('close', 'open')",)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=60)
     */
    private $token;

    public function __construct()
    {
        $this->links = new \Doctrine\Common\Collections\ArrayCollection();

        $this->status = self::STATUS_JOB_OPEN;
        $this->token = uniqid();
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Job
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Job
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set location.
     *
     * @param string $location
     *
     * @return Job
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location.
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Job
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipcode.
     *
     * @param string $zipcode
     *
     * @return Job
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode.
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set companyName.
     *
     * @param string $companyName
     *
     * @return Job
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName.
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set companyLogo.
     *
     * @param string $companyLogo
     *
     * @return Job
     */
    public function setCompanyLogo($companyLogo)
    {
        $this->companyLogo = $companyLogo;

        return $this;
    }

    /**
     * Get companyLogo.
     *
     * @return string
     */
    public function getCompanyLogo()
    {
        return $this->companyLogo;
    }

    /**
     * Set backgroundColor.
     *
     * @param string $backgroundColor
     *
     * @return Job
     */
    public function setBackgroundColor($backgroundColor)
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    /**
     * Get backgroundColor.
     *
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Job
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Job
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set backgroundTexture.
     *
     * @param \AppBundle\Entity\BackgroundTexture $backgroundTexture
     *
     * @return Job
     */
    public function setBackgroundTexture(\AppBundle\Entity\BackgroundTexture $backgroundTexture = null)
    {
        $this->backgroundTexture = $backgroundTexture;

        return $this;
    }

    /**
     * Get backgroundTexture.
     *
     * @return \AppBundle\Entity\BackgroundTexture
     */
    public function getBackgroundTexture()
    {
        return $this->backgroundTexture;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setCompanyLogoFile(File $logo = null)
    {
        $this->companyLogoFile = $logo;

        if ($logo) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getCompanyLogoFile()
    {
        return $this->companyLogoFile;
    }

    /**
     * Add links.
     *
     * @param \AppBundle\Entity\JobLink $links
     *
     * @return Job
     */
    public function addLink(\AppBundle\Entity\JobLink $links)
    {
        $this->links[] = $links;

        return $this;
    }

    /**
     * Remove links.
     *
     * @param \AppBundle\Entity\JobLink $links
     */
    public function removeLink(\AppBundle\Entity\JobLink $links)
    {
        $this->links->removeElement($links);
    }

    /**
     * Get links.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Job
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function isOpen()
    {
        return self::STATUS_JOB_OPEN === $this->status;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\Company $owner
     *
     * @return Job
     */
    public function setOwner(\AppBundle\Entity\Company $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\Company
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set token
     *
     * @param guid $token
     *
     * @return Job
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return guid
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Add question
     *
     * @param \AppBundle\Entity\Question $question
     *
     * @return Job
     */
    public function addQuestion(\AppBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \AppBundle\Entity\Question $question
     */
    public function removeQuestion(\AppBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }


    /**
     * Add questionsVideo
     *
     * @param \AppBundle\Entity\QuestionVideo $questionsVideo
     *
     * @return Job
     */
    public function addQuestionsVideo(\AppBundle\Entity\QuestionVideo $questionsVideo)
    {
        $this->questionsVideo[] = $questionsVideo;

        return $this;
    }

    /**
     * Remove questionsVideo
     *
     * @param \AppBundle\Entity\QuestionVideo $questionsVideo
     */
    public function removeQuestionsVideo(\AppBundle\Entity\QuestionVideo $questionsVideo)
    {
        $this->questionsVideo->removeElement($questionsVideo);
    }

    /**
     * Get questionsVideo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionsVideo()
    {
        return $this->questionsVideo;
    }

    public function getFilterableQuestionsCount()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->in('type', [
                Question::TYPE_RADIO,
                Question::TYPE_CHECKBOX,
                Question::TYPE_DROPDOWN,
            ]))
        ;

        return $this->questions->matching($criteria)->count();
    }

    public function getSubjectiveQuestionsCount()
    {
        $criteria = Criteria::create()
            ->where(
                Criteria::expr()->in('type', [
                    Question::TYPE_SHORT_ANSWER,
                    Question::TYPE_PARAGRAPH,
                ])
            )
        ;

        return $this->questions->matching($criteria)->count();

    }

    public function getQuestionsRemaining()
    {
        return self::QUESTIONS_LIMIT - $this->getQuestions()->count();
    }

    /**
     * Add applicant
     *
     * @param \AppBundle\Entity\Applicant $applicant
     *
     * @return Job
     */
    public function addApplicant(\AppBundle\Entity\Applicant $applicant)
    {
        $this->applicants[] = $applicant;

        return $this;
    }

    /**
     * Remove applicant
     *
     * @param \AppBundle\Entity\Applicant $applicant
     */
    public function removeApplicant(\AppBundle\Entity\Applicant $applicant)
    {
        $this->applicants->removeElement($applicant);
    }

    /**
     * Get applicants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicants()
    {
        return $this->applicants;
    }

    /**
     * Add document
     *
     * @param \AppBundle\Entity\JobDocument $document
     *
     * @return Job
     */
    public function addDocument(\AppBundle\Entity\JobDocument $document)
    {
        $document->setJob($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\JobDocument $document
     */
    public function removeDocument(\AppBundle\Entity\JobDocument $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    public function isDocumentTypeSelected($type)
    {
        $criteria = Criteria::create()
            ->where(
                Criteria::expr()->eq('documentType', $type)
            )
        ;

        return (bool) $this->documents->matching($criteria)->count();
    }

    public function getHiredApplicants()
    {
        $criteria = Criteria::create()
            ->where(
                Criteria::expr()->eq('hired', Applicant::STATUS_HIRED)
            )
        ;

        return $this->applicants->matching($criteria);
    }

    public function getInvitedApplicants()
    {
        $criteria = Criteria::create()
            ->where(
                Criteria::expr()->eq('invited', Applicant::STATUS_INVITED_YES)
            )
        ;

        return $this->applicants->matching($criteria);
    }

    public function getInterviewCompletedApplicants()
    {
        $criteria = Criteria::create()
            ->where(
                Criteria::expr()->eq('interviewCompleted', Applicant::INTERVIEW_COMPLETED_YES)
            )
        ;

        return $this->applicants->matching($criteria);
    }

    /**
     * Set companyLogoPreview
     *
     * @param string $companyLogoPreview
     *
     * @return Job
     */
    public function setCompanyLogoPreview($companyLogoPreview)
    {
        $this->companyLogoPreview = $companyLogoPreview;

        return $this;
    }

    /**
     * Get companyLogoPreview
     *
     * @return string
     */
    public function getCompanyLogoPreview()
    {
        return $this->companyLogoPreview;
    }

    /**
     * Set backgroundColorPreview
     *
     * @param string $backgroundColorPreview
     *
     * @return Job
     */
    public function setBackgroundColorPreview($backgroundColorPreview)
    {
        $this->backgroundColorPreview = $backgroundColorPreview;

        return $this;
    }

    /**
     * Get backgroundColorPreview
     *
     * @return string
     */
    public function getBackgroundColorPreview()
    {
        return $this->backgroundColorPreview;
    }

    /**
     * Set backgroundTexturePreview
     *
     * @param \AppBundle\Entity\BackgroundTexture $backgroundTexturePreview
     *
     * @return Job
     */
    public function setBackgroundTexturePreview(\AppBundle\Entity\BackgroundTexture $backgroundTexturePreview = null)
    {
        $this->backgroundTexturePreview = $backgroundTexturePreview;

        return $this;
    }

    /**
     * Get backgroundTexturePreview
     *
     * @return \AppBundle\Entity\BackgroundTexture
     */
    public function getBackgroundTexturePreview()
    {
        return $this->backgroundTexturePreview;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setCompanyLogoFilePreview(File $logo = null)
    {
        $this->companyLogoFilePreview = $logo;

        if ($logo) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getCompanyLogoFilePreview()
    {
        return $this->companyLogoFilePreview;
    }

    public function _clone()
    {
        $this->id = null;
        $this->createdAt = new \DateTime('now');
        return $this;
    }
}
