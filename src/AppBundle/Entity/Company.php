<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Table(name="`hs_company`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 */
class Company
{
    const PAYMENT_TYPE_CREDIT_CARD = 'credit_card';
    const PAYMENT_TYPE_PAYPAL      = 'paypal';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="CreditCard", mappedBy="company", cascade={"ALL", "MERGE"})
     */
    private $creditCard;

    /**
     * @ORM\OneToMany(targetEntity="Employer", mappedBy="company", cascade={"ALL"})
     * @Assert\Valid
     */
    private $employers;

    /**
     * @ORM\OneToMany(targetEntity="Job", mappedBy="owner", cascade={"ALL"})
     * @Assert\Valid
     */
    private $jobs;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=10)
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", columnDefinition="ENUM('credit_card', 'paypal')", nullable=false)
     */
    private $paymentType;

    /**
     * @var string
     *
     * @ORM\Column(name="paypal_account", type="string", length=255, nullable=true)
     * @Assert\Email
     */
    private $paypalAccount;

    /**
     * @ORM\ManyToOne(targetEntity="PricingModel")
     * @ORM\JoinColumn(name="pricing_model_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $pricingModel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->employers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->paymentType = self::PAYMENT_TYPE_CREDIT_CARD;
    }

    public static function getPaymentTypeChoice()
    {
        return [
            self::PAYMENT_TYPE_CREDIT_CARD => 'form.register.company.paymentType.credit_card',
            self::PAYMENT_TYPE_PAYPAL => 'form.register.company.paymentType.paypal',
        ];
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Company
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set pricingModel
     *
     * @param \AppBundle\Entity\PricingModel $pricingModel
     * @return Company
     */
    public function setPricingModel(\AppBundle\Entity\PricingModel $pricingModel)
    {
        $this->pricingModel = $pricingModel;

        return $this;
    }

    /**
     * Get pricingModel
     *
     * @return \AppBundle\Entity\PricingModel
     */
    public function getPricingModel()
    {
        return $this->pricingModel;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Company
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Company
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add employers
     *
     * @param \AppBundle\Entity\Employer $employers
     * @return Company
     */
    public function addEmployer(\AppBundle\Entity\Employer $employers)
    {
        $this->employers[] = $employers;

        return $this;
    }

    /**
     * Remove employers
     *
     * @param \AppBundle\Entity\Employer $employers
     */
    public function removeEmployer(\AppBundle\Entity\Employer $employers)
    {
        $this->employers->removeElement($employers);
    }

    /**
     * Get employers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployers()
    {
        return $this->employers;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return Company
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set paymentType
     *
     * @param string $paymentType
     *
     * @return Company
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Set paypalAccount
     *
     * @param string $paypalAccount
     *
     * @return Company
     */
    public function setPaypalAccount($paypalAccount)
    {
        $this->paypalAccount = $paypalAccount;

        return $this;
    }

    /**
     * Get paypalAccount
     *
     * @return string
     */
    public function getPaypalAccount()
    {
        return $this->paypalAccount;
    }

    /**
     * Set creditCard
     *
     * @param \AppBundle\Entity\CreditCard $creditCard
     *
     * @return Company
     */
    public function setCreditCard(\AppBundle\Entity\CreditCard $creditCard = null)
    {
        $this->creditCard = $creditCard;

        return $this;
    }

    /**
     * Get creditCard
     *
     * @return \AppBundle\Entity\CreditCard
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }

    public function isPaypalAsPayment()
    {
        return $this->paymentType === self::PAYMENT_TYPE_PAYPAL;
    }

    /**
     * Add job
     *
     * @param \AppBundle\Entity\Job $job
     *
     * @return Company
     */
    public function addJob(\AppBundle\Entity\Job $job)
    {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove job
     *
     * @param \AppBundle\Entity\Job $job
     */
    public function removeJob(\AppBundle\Entity\Job $job)
    {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }
}
