<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="`hs_pricing_model_configuration`", uniqueConstraints={@ORM\UniqueConstraint(name="PRICING_MODEL_FEATURE_IDX", columns={"pricing_model_id", "pricing_model_feature_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PricingModelConfigurationRepository")
 */
class PricingModelConfiguration
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PricingModel", inversedBy="features")
     * @ORM\JoinColumn(name="pricing_model_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $pricingModel;

    /**
     * @ORM\ManyToOne(targetEntity="PricingModelFeature")
     * @ORM\JoinColumn(name="pricing_model_feature_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $pricingModelFeature;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function __toString()
    {
        return sprintf('%s - %s', (string) $this->getPricingModel(), (string) $this->getPricingModelFeature());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return PricingModelConfiguration
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PricingModelConfiguration
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return PricingModelConfiguration
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set pricingModel
     *
     * @param \AppBundle\Entity\PricingModel $pricingModel
     * @return PricingModelConfiguration
     */
    public function setPricingModel(\AppBundle\Entity\PricingModel $pricingModel)
    {
        $this->pricingModel = $pricingModel;

        return $this;
    }

    /**
     * Get pricingModel
     *
     * @return \AppBundle\Entity\PricingModel
     */
    public function getPricingModel()
    {
        return $this->pricingModel;
    }

    /**
     * Set pricingModelFeature
     *
     * @param \AppBundle\Entity\PricingModelFeature $pricingModelFeature
     * @return PricingModelConfiguration
     */
    public function setPricingModelFeature(\AppBundle\Entity\PricingModelFeature $pricingModelFeature)
    {
        $this->pricingModelFeature = $pricingModelFeature;

        return $this;
    }

    /**
     * Get pricingModelFeature
     *
     * @return \AppBundle\Entity\PricingModelFeature
     */
    public function getPricingModelFeature()
    {
        return $this->pricingModelFeature;
    }
}
