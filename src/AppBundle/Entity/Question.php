<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="`hs_question`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 */
class Question
{
    const STATUS_DRAFT     =  'draft';
    const STATUS_PUBLISHED =  'published';

    const TYPE_RADIO        = 'radio';
    const TYPE_CHECKBOX     = 'checkbox';
    const TYPE_DROPDOWN     = 'dropdown';
    const TYPE_SHORT_ANSWER = 'short_answer';
    const TYPE_PARAGRAPH    = 'paragraph';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="QuestionAnswer", mappedBy="question", cascade={"all"}, orphanRemoval=true, fetch="EAGER")
     */
    private $answers;

    /**
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="question", cascade={"all"}, orphanRemoval=true)
     */
    private $applicantAnswers;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    private $question;

    /**
     * @var array
     *
     * @ORM\Column(name="question_metadata", type="array")
     */
    private $questionMetadata;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string",  columnDefinition="ENUM('draft', 'published')", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="candidate_instruction", type="string", length=255, nullable=true)
     */
    private $candidateInstruction;

     /**
     * @ORM\ManyToOne(targetEntity="Job", inversedBy="questions")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    private $job;

    /**
     * @var integer
     *
     * @ORM\Column(name="`order`", type="integer", length=255)
     */
    private $order;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public static function getAllTypes()
    {
        return [
            self::TYPE_RADIO,
            self::TYPE_CHECKBOX,
            self::TYPE_DROPDOWN,
            self::TYPE_SHORT_ANSWER,
            self::TYPE_PARAGRAPH,
        ];
    }

    public static function getMultipleChoicesTypes()
    {
        return [
            self::TYPE_RADIO,
            self::TYPE_CHECKBOX,
            self::TYPE_DROPDOWN,
        ];
    }

    public static function getAllTypesLabels()
    {
        return [
            self::TYPE_RADIO => 'form.add_question.type.radio',
            self::TYPE_CHECKBOX => 'form.add_question.type.checkbox',
            self::TYPE_DROPDOWN => 'form.add_question.type.dropdown',
            self::TYPE_SHORT_ANSWER => 'form.add_question.type.short_answer',
            self::TYPE_PARAGRAPH => 'form.add_question.type.paragraph',
        ];
    }

    public function getTypeLabel()
    {
        return self::getAllTypesLabels()[$this->getType()];
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->question;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Question
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set questionMetadata
     *
     * @param array $questionMetadata
     * @return Question
     */
    public function setQuestionMetadata($questionMetadata)
    {
        $this->questionMetadata = $questionMetadata;

        return $this;
    }

    /**
     * Get questionMetadata
     *
     * @return array
     */
    public function getQuestionMetadata()
    {
        return $this->questionMetadata;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Question
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set candidateInstruction
     *
     * @param string $candidateInstruction
     * @return Question
     */
    public function setCandidateInstruction($candidateInstruction)
    {
        $this->candidateInstruction = $candidateInstruction;

        return $this;
    }

    /**
     * Get candidateInstruction
     *
     * @return string
     */
    public function getCandidateInstruction()
    {
        return $this->candidateInstruction;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Question
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Question
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set job
     *
     * @param \AppBundle\Entity\Job $job
     * @return Question
     */
    public function setJob(\AppBundle\Entity\Job $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \AppBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Question
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\QuestionAnswer $answer
     *
     * @return Question
     */
    public function addAnswer(\AppBundle\Entity\QuestionAnswer $answer)
    {
        $answer->setQuestion($this);
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\QuestionAnswer $answer
     */
    public function removeAnswer(\AppBundle\Entity\QuestionAnswer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add applicantAnswer
     *
     * @param \AppBundle\Entity\Answer $applicantAnswer
     *
     * @return Question
     */
    public function addApplicantAnswer(\AppBundle\Entity\Answer $applicantAnswer)
    {
        $this->applicantAnswers[] = $applicantAnswer;

        return $this;
    }

    /**
     * Remove applicantAnswer
     *
     * @param \AppBundle\Entity\Answer $applicantAnswer
     */
    public function removeApplicantAnswer(\AppBundle\Entity\Answer $applicantAnswer)
    {
        $this->applicantAnswers->removeElement($applicantAnswer);
    }

    /**
     * Get applicantAnswers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicantAnswers()
    {
        return $this->applicantAnswers;
    }
}
