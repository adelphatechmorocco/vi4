<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Mapping\Annotation as Gedmo;

use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="`hs_applicant_video`", uniqueConstraints={@ORM\UniqueConstraint(name="APPLICANT_QUESTION_VIDEO_IDX", columns={"question_video_id", "applicant_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicantVideoRepository")
 * @Vich\Uploadable
 */
class ApplicantVideo
{

    const VIDEO_CONVERTED     =  'converted';
    const VIDEO_NOT_CONVERTED  =  'not_converted';
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="QuestionVideo", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="question_video_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $questionVideo;

    /**
    * @ORM\ManyToOne(targetEntity="Applicant", cascade={"persist"}, inversedBy="videos")
    * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id", nullable=false)
    * @Assert\NotBlank
    * @Assert\Valid
    */
    private $applicant;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=255)
     */
    private $video;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="applicant_video", fileNameProperty="video")
     * @Assert\File(
     *     maxSize = "10M",
     *     mimeTypes = {
     *      "multipart/x-mixed-replace",
     *      "video/mp4",
     *      "video/webm",
     *     }
     * )
     */
    private $videoFile;

    /**
     * @var string
     *
     * @ORM\Column(name="written_answer", type="text", nullable=true)
     */
    private $writtenAnswer;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate", type="smallint", nullable=true))
     * @Assert\Range(
     *      min = 0,
     *      max = 5,
     *      minMessage = "applicant_video.rate",
     *      maxMessage = "applicant_video.rate",
     *      groups = {"Default", "Rate"}
     * )
     */
    private $rate = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="converted", type="string", columnDefinition="ENUM('converted', 'not_converted')", nullable=true)
     */
    private $converted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->converted = self::VIDEO_NOT_CONVERTED;
    }

    public function __toString()
    {
        return $this->video;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set video
     *
     * @param string $video
     * @return ApplicantVideo
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $video
     */
    public function setVideoFile(File $video = null)
    {
        $this->videoFile = $video;

        if ($video) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getVideoFile()
    {
        return $this->videoFile;
    }

    /**
     * Set writtenAnswer
     *
     * @param string $writtenAnswer
     * @return ApplicantVideo
     */
    public function setWrittenAnswer($writtenAnswer)
    {
        $this->writtenAnswer = $writtenAnswer;

        return $this;
    }

    /**
     * Get writtenAnswer
     *
     * @return string
     */
    public function getWrittenAnswer()
    {
        return $this->writtenAnswer;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ApplicantVideo
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ApplicantVideo
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set questionVideo
     *
     * @param \AppBundle\Entity\QuestionVideo $questionVideo
     * @return ApplicantVideo
     */
    public function setQuestionVideo(\AppBundle\Entity\QuestionVideo $questionVideo)
    {
        $this->questionVideo = $questionVideo;

        return $this;
    }

    /**
     * Get questionVideo
     *
     * @return \AppBundle\Entity\QuestionVideo
     */
    public function getQuestionVideo()
    {
        return $this->questionVideo;
    }

    /**
     * Set applicant
     *
     * @param \AppBundle\Entity\Applicant $applicant
     * @return ApplicantVideo
     */
    public function setApplicant(\AppBundle\Entity\Applicant $applicant)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return \AppBundle\Entity\Applicant
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     *
     * @return ApplicantVideo
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return ApplicantVideo
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set converted
     *
     * @param string $converted
     *
     * @return ApplicantVideo
     */
    public function setConverted($converted)
    {
        $this->converted = $converted;

        return $this;
    }

    /**
     * Get converted
     *
     * @return string
     */
    public function getConverted()
    {
        return $this->converted;
    }
}
