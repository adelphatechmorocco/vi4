<?php

namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="`hs_applicant`", uniqueConstraints={@ORM\UniqueConstraint(name="APPLICANT_EMAIL_JOB_IDX", columns={"email", "job_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicantRepository")
 * @Vich\Uploadable
 * @UniqueEntity(
 *     fields={"email", "job"},
 *     errorPath="email",
 *     message="job.applicant.already_apply"
 * )
 */
class Applicant
{
    const STATUS_INVITED_YES =  'yes';
    const STATUS_INVITED_NO  =  'no';

    const INTERVIEW_COMPLETED_YES =  'yes';
    const INTERVIEW_COMPLETED_NO  =  'no';

    const STATUS_HIRED     =  'hired';
    const STATUS_NOT_HIRED =  'not_hired';

    const ADVANCED_YES   =  'yes';
    const ADVANCED_NO    =  'no';
    const ADVANCED_MAYBE =  'maybe';

    const INTERVIEW_DEADLINE = '3 days';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="applicant", cascade={"all"}, fetch="EAGER")
     */
    private $answers;

    /**
     * @ORM\OneToMany(targetEntity="ApplicantVideo", mappedBy="applicant", cascade={"all"})
     */
    private $videos;

    /**
     * @ORM\OneToMany(targetEntity="ApplicantDocument", mappedBy="applicant", cascade={"all"})
     */
    private $documents;

    /**
     * @ORM\ManyToOne(targetEntity="Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    private $job;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     * @Assert\NotBlank
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     * @Assert\NotBlank
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=500)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=10)
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true))
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate", type="smallint", nullable=true))
     * @Assert\Range(
     *      min = 0,
     *      max = 5,
     *      minMessage = "applicant.rate_range",
     *      maxMessage = "applicant.rate_range",
     *      groups = {"Default", "Rate"}
     * )
     */
    private $rate = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="invite", type="string", columnDefinition="ENUM('yes', 'no')", nullable=true)
     */
    private $invited;

    /**
     * @var string
     *
     * @ORM\Column(name="hired", type="string", columnDefinition="ENUM('hired', 'not_hired')", nullable=true)
     */
    private $hired;

    /**
     * @var string
     *
     * @ORM\Column(name="advanced", type="string", columnDefinition="ENUM('yes', 'no', 'maybe')", nullable=true)
     * @Assert\Choice(callback="getAdvancedChoices", groups={"Default", "Advanced"})
     */
    private $advanced;

    /**
     * @var string
     *
     * @ORM\Column(name="interview_completed", type="string", columnDefinition="ENUM('yes', 'no')", nullable=true)
     */
    private $interviewCompleted;

    /**
     * @var string
     *
     * @ORM\Column(name="reffered", type="string", columnDefinition="ENUM('yes', 'no')", nullable=true)
     */
    private $reffered;

    /**
     * @var string
     *
     * @ORM\Column(name="referral_description", type="string", length=1024, nullable=true)
     */
    private $referralDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=100, nullable=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="applicant_picture", type="string", length=255, nullable=true)
     */
    private $applicantPicture;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="job_applicant_picture", fileNameProperty="applicantPicture")
     * @Assert\File(
     *     maxSize = "2M",
     *     mimeTypes = {
     *         "image/jpg",
     *         "image/jpeg",
     *         "image/pjpeg",
     *         "image/png",
     *     }
     * )
     */
    private $applicantPictureFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invited_date", type="datetime", nullable=true)
     */
    private $invitedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->invited = self::STATUS_INVITED_NO;
        $this->hired   = self::STATUS_NOT_HIRED;
        $this->interviewCompleted = self::INTERVIEW_COMPLETED_NO;
    }

    public function __toString()
    {
        return $this->getFullname();
    }

    public static function getAdvancedChoices()
    {
        return [
            self::ADVANCED_YES,
            self::ADVANCED_NO,
            self::ADVANCED_MAYBE,
        ];
    }

    public static function getAdvancedChoicesLabels()
    {
        return [
            self::ADVANCED_YES   => 'Yes',
            self::ADVANCED_NO    => 'No',
            self::ADVANCED_MAYBE => 'Maybe',
        ];
    }

    public function getFullname()
    {
        return sprintf('%s %s', $this->firstname, $this->lastname);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Applicant
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Applicant
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Applicant
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Applicant
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Applicant
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return Applicant
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Applicant
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     * @return Applicant
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set invited
     *
     * @param string $invited
     * @return Applicant
     */
    public function setInvited($invited)
    {
        $this->invited = $invited;

        return $this;
    }

    /**
     * Get invited
     *
     * @return string
     */
    public function getInvited()
    {
        return $this->invited;
    }

    /**
     * IS invited
     *
     * @return  Boolean
     */
    public function isInvited()
    {
        return $this->invited === self::STATUS_INVITED_YES;
    }

    /**
     * Set hired
     *
     * @param string $hired
     * @return Applicant
     */
    public function setHired($hired)
    {
        $this->hired = $hired;

        return $this;
    }

    /**
     * Get hired
     *
     * @return string
     */
    public function getHired()
    {
        return $this->hired;
    }

    /**
     * Is hired
     *
     * @return Boolean
     */
    public function isHired()
    {
        return $this->hired === self::STATUS_HIRED;
    }

    /**
     * Set advanced
     *
     * @param string $advanced
     * @return Applicant
     */
    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;

        return $this;
    }

    /**
     * Get advanced
     *
     * @return string
     */
    public function getAdvanced()
    {
        return $this->advanced;
    }

    public function getAdvancedLabel()
    {
        return self::getAdvancedChoicesLabels()[$this->advanced];
    }

    /**
     * Set referralDescription
     *
     * @param string $referralDescription
     * @return Applicant
     */
    public function setReferralDescription($referralDescription)
    {
        $this->referralDescription = $referralDescription;

        return $this;
    }

    /**
     * Get referralDescription
     *
     * @return string
     */
    public function getReferralDescription()
    {
        return $this->referralDescription;
    }

    /**
     * Set job
     *
     * @param \AppBundle\Entity\Job $job
     * @return Applicant
     */
    public function setJob(\AppBundle\Entity\Job $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \AppBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Applicant
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Applicant
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Applicant
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\Answer $answer
     *
     * @return Applicant
     */
    public function addAnswer(\AppBundle\Entity\Answer $answer)
    {
        $answer->setApplicant($this);
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\Answer $answer
     */
    public function removeAnswer(\AppBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add document
     *
     * @param \AppBundle\Entity\ApplicantDocument $document
     *
     * @return Applicant
     */
    public function addDocument(\AppBundle\Entity\ApplicantDocument $document)
    {
        $this->documents[] = $document;
        $document->setApplicant($this);

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\ApplicantDocument $document
     */
    public function removeDocument(\AppBundle\Entity\ApplicantDocument $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Add video
     *
     * @param \AppBundle\Entity\ApplicantVideo $video
     *
     * @return Applicant
     */
    public function addVideo(\AppBundle\Entity\ApplicantVideo $video)
    {
        $video->setApplicant($this);
        $this->videos[] = $video;

        return $this;
    }

    /**
     * Remove video
     *
     * @param \AppBundle\Entity\ApplicantVideo $video
     */
    public function removeVideo(\AppBundle\Entity\ApplicantVideo $video)
    {
        $this->videos->removeElement($video);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    public function getNextQuestionVideoToInterview()
    {
        $questionsVideosInterviewed = array_map(function($applicantVideo) {
            return $applicantVideo->getQuestionVideo()->getId();

        }, $this->videos->toArray());
        foreach ($this->job->getQuestionsVideo() as $key=>$questionVideo) {
            if (!in_array($questionVideo->getId(), $questionsVideosInterviewed)) {
                return [
                    'questionVideo'=>$questionVideo,
                    'count' => $key
                ];
            }
        }

        return false;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Applicant
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return Applicant
     */
    public function setSource($source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set interviewCompleted
     *
     * @param string $interviewCompleted
     *
     * @return Applicant
     */
    public function setInterviewCompleted($interviewCompleted)
    {
        $this->interviewCompleted = $interviewCompleted;

        return $this;
    }

    /**
     * Get interviewCompleted
     *
     * @return string
     */
    public function getInterviewCompleted()
    {
        return $this->interviewCompleted;
    }

    /**
     * Set reffered
     *
     * @param string $reffered
     *
     * @return Applicant
     */
    public function setReffered($reffered)
    {
        $this->reffered = $reffered;

        return $this;
    }

    /**
     * Get reffered
     *
     * @return string
     */
    public function getReffered()
    {
        return $this->reffered;
    }

    /**
     * Set applicantPicture
     *
     * @param string $applicantPicture
     *
     * @return Applicant
     */
    public function setApplicantPicture($applicantPicture)
    {
        $this->applicantPicture = $applicantPicture;

        return $this;
    }

    /**
     * Get applicantPicture
     *
     * @return string
     */
    public function getApplicantPicture()
    {
        return $this->applicantPicture;
    }
    
     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setApplicantPictureFile(File $image = null)
    {
        $this->applicantPictureFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getApplicantPictureFile()
    {
        return $this->applicantPictureFile;
    }

    /**
     * Set invitedDate
     *
     * @param \DateTime $invitedDate
     *
     * @return Applicant
     */
    public function setInvitedDate($invitedDate)
    {
        $this->invitedDate = $invitedDate;

        return $this;
    }

    /**
     * Get invitedDate
     *
     * @return \DateTime
     */
    public function getInvitedDate()
    {
        return $this->invitedDate;
    }
}
