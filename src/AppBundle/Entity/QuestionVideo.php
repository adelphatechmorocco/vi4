<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="`hs_question_video`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionVideoRepository")
 */
class QuestionVideo
{
	const DURATION_10_SECONDS = 10;
	const DURATION_30_SECONDS = 30;
	const DURATION_60_SECONDS = 60;
	const DURATION_90_SECONDS = 90;
	const DURATION_120_SECONDS = 120;
	const DURATION_150_SECONDS = 150;
	const DURATION_180_SECONDS = 180;

	const TIME_READ_QUESTION_5  = 05;
	const TIME_READ_QUESTION_10 = 10;
	const TIME_READ_QUESTION_15 = 15;
	const TIME_READ_QUESTION_20 = 20;

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Job", inversedBy="questionsVideo")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * @Assert\Valid
     */
    private $job;

     /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=500)
     * @Assert\NotBlank
     */
    private $question;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="smallint")
     * @Assert\NotBlank
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="time_read_question", type="smallint")
     * @Assert\NotBlank
     */
    private $timeReadQuestion;

    /**
     * @var integer
     *
     * @ORM\Column(name="`order`", type="integer", length=255, nullable=true)
     */
    private $order;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function __toString()
    {
        return $this->question;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return QuestionVideo
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return QuestionVideo
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return QuestionVideo
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return QuestionVideo
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set job
     *
     * @param \AppBundle\Entity\Job $job
     * @return QuestionVideo
     */
    public function setJob(\AppBundle\Entity\Job $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \AppBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->applicantsVideo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add applicantsVideo
     *
     * @param \AppBundle\Entity\ApplicantVideo $applicantsVideo
     *
     * @return QuestionVideo
     */
    public function addApplicantsVideo(\AppBundle\Entity\ApplicantVideo $applicantsVideo)
    {
        $this->applicantsVideo[] = $applicantsVideo;

        return $this;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return QuestionVideo
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set timeReadQuestion
     *
     * @param integer $timeReadQuestion
     *
     * @return QuestionVideo
     */
    public function setTimeReadQuestion($timeReadQuestion)
    {
        $this->timeReadQuestion = $timeReadQuestion;

        return $this;
    }

    /**
     * Get timeReadQuestion
     *
     * @return integer
     */
    public function getTimeReadQuestion()
    {
        return $this->timeReadQuestion;
    }
}
