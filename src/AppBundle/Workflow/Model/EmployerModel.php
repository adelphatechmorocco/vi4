<?php

namespace AppBundle\Workflow\Model;

use Lexik\Bundle\WorkflowBundle\Model\ModelInterface;
use AppBundle\Entity\Employer;

class EmployerModel implements ModelInterface
{
	private $employer;

    public function __construct(Employer $employer)
    {
        $this->employer = $employer;
    }

    public function getEmployer()
    {
    	return $this->employer;
    }

    public function setStep($step)
    {
        $this->employer->setStep($step);
    }

    public function getStep()
    {
        return $this->employer->getStep();
    }

    public function getWorkflowIdentifier()
    {
        return md5(get_class($this->employer).'-'.$this->employer->getId());
    }

    public function getWorkflowData()
    {
        return [
            'employer_id' => $this->employer->getId(),
        ];
    }

    public function getWorkflowObject()
    {
        return $this->employer;
    }
}
