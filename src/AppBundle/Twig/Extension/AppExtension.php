<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Utils\Utils;

class AppExtension extends \Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('human_file_size', [$this, 'humanFileSize']),
        ];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('file_exists', 'file_exists'),
        ];
    }

    public function humanFileSize($bytes, $decimals = 2)
    {
        return Utils::humanFileSize($bytes, $decimals);
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'app_extension';
    }
}
