<?php

namespace AppBundle\Controller\Employer;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Employer;
use AppBundle\Entity\Company;
use AppBundle\Entity\CreditCard;
use AppBundle\Entity\Job;
use AppBundle\Form\Type\EmployerAccountBillingType;
use OpauthBundle\Profile;

/**
 * @Route("/dashboard")
 */
class AccountController extends Controller
{
    /**
     * @Route("/register/pricing" , name="employer_register_model_pricing")
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function modelPricingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $modelsPricing = $em->getRepository('AppBundle:PricingModel')->findBy([], ['order' => 'ASC']);

        if ($request->isMethod('POST')) {
            if ($request->request->has('pp')) {
                $pricingModel = $em->getRepository('AppBundle:PricingModel')->find($request->request->getInt('pp'));
                if (!$pricingModel) {
                    throw $this->createNotFoundException();
                }

                return $this->redirectToRoute('employer_register_account_billing', [
                    'pp' => $pricingModel->getId(),
                ]);

            }

            $this->addFlash('error', 'flash.message.chooce_pricing_model');

            return $this->redirectToRoute('employer_register_model_pricing');
        }

        return $this->render('Signup/Step/second.html.twig', [
            'modelsPricing' => $modelsPricing,
        ]);
    }

	/**
     * @Route("/register/account-billing/{pp}", name="employer_register_account_billing")
     * @Security("has_role('ROLE_EMPLOYER')")
     */
	public function accountBillingAction(Request $request)
	{
        $em = $this->getDoctrine()->getManager();
        if ($request->attributes->has('pp')) {
            $pricingModel = $em->getRepository('AppBundle:PricingModel')->find($request->attributes->getInt('pp'));
            if (!$pricingModel) {
                $this->addFlash('error', 'flash.message.chooce_pricing_model');

                return $this->redirectToRoute('employer_register_model_pricing');
            }
        }

        $user = $this->getUser();

        $company = $user->getCompany() ?: new Company();
        $creditCard = $company->getCreditCard() ?: new CreditCard();

        $user
            ->setCompany($company)
            ->getCompany()
            ->addEmployer($user)
            ->setCreditCard($creditCard)
            ->getCreditCard()
            ->setCompany($company)
        ;

        $form = $this->createForm(new EmployerAccountBillingType, $user, [
            'pricingModel' => $pricingModel,
            'profile' => $request->getSession()->get('user_profile') ?: new Profile(),
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($company->isPaypalAsPayment()) {
                $company->setCreditCard(null);
            }

            $user->setStep(Employer::STEP_ACCOUNT_BILLING);
            $em->flush();

            return $this->redirectToRoute('dashboard_homepage');
        }

		return $this->render('Signup/Step/third.html.twig', [
            'form' => $form->createView(),
        ]);
	}

    /**
     * @Route("/", name="dashboard_homepage")
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function dashboardAction (Request $request)
    {
    	return $this->render('Dashboard/index.html.twig');
    }

}
