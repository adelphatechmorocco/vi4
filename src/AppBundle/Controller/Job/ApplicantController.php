<?php

namespace AppBundle\Controller\Job;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\ApplicantUpdateType;
use AppBundle\Entity\Job;
use AppBundle\Entity\Applicant;
use AppBundle\Entity\ApplicantVideo;
use AppBundle\Form\Type\ApplicantReviewType;
use AppBundle\Entity\ApplicantDocument;
use AppBundle\Form\Filter\ApplicantQuestionListingFilterType;

/**
 * @Route("/dashboard")
 */
class ApplicantController extends Controller
{
    /**
     * @Route("/job/{slug}/applicants", name="job_applicants")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function applicantsAction(Request $request, Job $job)
    {
        $em = $this->getDoctrine()->getManager();

        $filterForm = $this->get('form.factory')->create(new ApplicantQuestionListingFilterType(), $job);
        $filterForm->handleRequest($request);

        if ($filterForm->isSubmitted() && !$filterForm->isValid()) {
            throw $this->createNotFoundException();
        }

        $filters = [];
        if ($af = $request->query->get('af')) {
            if (!empty($af['questions'])) {
                foreach ($af['questions'] as $keyQuestion => $answer) {
                    if (null === $answer['a'] || '' === $answer['a']) {
                        continue;
                    }

                    $question = $job->getQuestions()->get($keyQuestion);

                    if (is_array($answer['a'])) {
                        foreach ($answer['a'] as $answerId) {
                            $answers[] = (int) $answerId;
                        }
                    } else {
                        $answers = (int) $answer['a'];
                    }

                    $question = $job->getQuestions()->get($keyQuestion);

                    $filters[] = [
                        'question' => $question,
                        'answers'  => $answers,
                    ];

                    unset($answers);
                }
            }
        }

        $filterBuilder = $em->getRepository('AppBundle:Applicant')->filterBuilder(
            $job,
            $request->query->get('advanced'),
            $filters
        );
        $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $filterBuilder);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $filterBuilder->getQuery(),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('Dashboard/Job/applicants.html.twig', [
            'pagination' => $pagination,
            'filterForm' => $filterForm->createView(),
            'job' => $job,
        ]);
    }
    
    /**
     * @Route("/job/{slug}/applicants/datas", name="job_applicant_datas", options={"expose"=true})
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
     public function datasAction(Request $request, Job $job)
     {
        $em    = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT a FROM AppBundle:Applicant a WHERE a.job = :job");
        $query->setParameter('job', $job);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

         return $this->render('Dashboard/Job/datas.html.twig', [
            'applicants' => $pagination,
            'job' => $job,
        ]);
     }

    /**
     * @Route("/job/{slug}/applicant/{applicantId}", name="job_applicant", options={"expose"=true})
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("applicant", options={"mapping": {"applicantId": "id"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function applicantAction(Request $request, Job $job, Applicant $applicant, $currentURL = null)
    {
        $form = $this->createForm(new ApplicantUpdateType, $applicant, [
            'currentURL' => $currentURL,
        ]);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                if (Applicant::ADVANCED_NO === $applicant->getAdvanced()) {
                    $this->get('sylius.email_sender')->send('applicant_rejection_advanced_no', [
                        $applicant->getEmail() ,
                    ], [
                        'applicant' => $applicant,
                    ]);
                }
            }

            return $this->redirect(
                $form['currentURL']->getData() ?: $this->generateUrl('job_applicants', ['slug' => $job->getSlug()])
            );
        }

        return $this->render('Dashboard/Job/_applicant.html.twig', [
            'formApplicant' => $form->createView(),
            'job' => $job,
            'applicant' => $applicant,
        ]);
    }

    /**
     * @Route("/job/{slug}/applicant/{applicantId}/hired", name="job_applicant_hired",options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("applicant", options={"mapping": {"applicantId": "id"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function hireAction(Request $request, Job $job, Applicant $applicant)
    {
        $status = Applicant::STATUS_HIRED;
        if ($applicant->isHired()) {
            $status = Applicant::STATUS_NOT_HIRED;
        }

        $em = $this->getDoctrine()->getManager();
        $applicant->setHired($status);

        $em->persist($applicant);
        $em->flush();


        $this->get('sylius.email_sender')->send('applicant_email_hired', [
            $applicant->getEmail() ,
        ], [
            'job' => $job
        ]);

        return new JsonResponse([
            'success' => true,
            'title' => $this->get('translator')->trans('flash.message.success.applicant.hired_status_changed'),
            'messages' => [],
        ]);
    }

    /**
     * @Route("/job/{slug}/applicant/{applicantId}/invite", name="job_applicant_invite", options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("applicant", options={"mapping": {"applicantId": "id"}})
     * Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function inviteAction(Request $request, Job $job, Applicant $applicant)
    {
        if (!$job->getQuestionsVideo()->count()) {
            return new JsonResponse([
                'success' => false,
                'title' => $this->get('translator')->trans('flash.message.error.job.no_questions_video'),
                'messages' => [],
            ]);
        }

        if ($applicant->isInvited()) {
            return new JsonResponse([
                'success' => true,
                'title' => $this->get('translator')->trans('flash.message.success.applicant.already_invited'),
                'messages' => [],
            ]);
        }

        $applicant
            ->setInvited(Applicant::STATUS_INVITED_YES)
            ->setInvitedDate(new \DateTime())
            ->setToken(
                $token = $this->container->get('fos_user.util.token_generator')->generateToken()
            )
        ;

        $em = $this->getDoctrine()->getManager();
        $em->persist($applicant);
        $em->flush();

        $deadline = date_add(new \DateTime('-3 hours'), date_interval_create_from_date_string(Applicant::INTERVIEW_DEADLINE));
        $this->get('sylius.email_sender')->send('applicant_email_invite', [
            $applicant->getEmail() ,
        ], [
            'applicant' => $applicant,
            'token' => $token,
            'deadline' => $deadline
        ]);

        return new JsonResponse([
            'success' => true,
            'title' => $this->get('translator')->trans('flash.message.success.applicant.invited'),
            'messages' => [],
        ]);
    }

    /**
     * @Route("/applicant/{id}/video", name="applicant_video", options={"expose"=true})
     * @ParamConverter("applicant", options={"mapping": {"id": "id"}})
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function videoAction(Request $request, Applicant $applicant)
    {
        $form = $this->createForm(new ApplicantReviewType, $applicant);
        $form->handleRequest($request);
        if ($request->isXmlHttpRequest()) { 
                $em = $this->getDoctrine()->getManager();
                $em->flush();
        }

        return $this->render('Dashboard/Job/_video.html.twig', [
            'applicant' => $applicant,
            'formReviewApplicant' => $form->createView(),
        ]);

    }

    /**
     * @Route("/job/{slug}/applicant/{applicantId}/document/{documentId}/download", name="job_applicant_document_download")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("applicant", options={"mapping": {"applicantId": "id"}})
     * @ParamConverter("applicantDocument", options={"mapping": {"documentId": "id"}})
     * Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function downloadDocumentAction(Request $request, Job $job, Applicant $applicant, ApplicantDocument $applicantDocument)
    {
        if ($applicant->getId() !== $applicantDocument->getApplicant()->getId()) {
            throw $this->createAccessDeniedException();
        }

        if ($job->getId() !== $applicantDocument->getApplicant()->getJob()->getId()) {
            throw $this->createAccessDeniedException();
        }

        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $documentPath = $this->get('kernel')->getRootDir() . '/../web' . $helper->asset($applicantDocument, 'documentFile');

        if (!$this->get('filesystem')->exists($documentPath)) {
            throw $this->createNotFoundException();
        }

        $response = new BinaryFileResponse($documentPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }
}
