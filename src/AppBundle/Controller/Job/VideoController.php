<?php

namespace AppBundle\Controller\Job;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use AppBundle\Form\Type\ApplicantVideoType;
use AppBundle\Entity\Applicant;
use AppBundle\Entity\ApplicantVideo;
use AppBundle\Entity\QuestionVideo;


class VideoController extends Controller
{
    /**
     * @Route("/applicant/{token}/video/interview", name="applicant_interview")
     * @ParamConverter("applicant", options={"mapping": {"token": "token"}})
     */
     public function interviewAction(Request $request, Applicant $applicant)
     {
         $dateNow = new \DateTime('-3 hours');
         $deadline = date_add($applicant->getInvitedDate(), date_interval_create_from_date_string(Applicant::INTERVIEW_DEADLINE));
         if($deadline < $dateNow){

            return $this->render('Job/Video/closed_interview.html.twig',[
                'applicant' => $applicant,
            ]);

         } else {

         return $this->render('Job/Video/interview.html.twig',[
                'applicant' => $applicant,
         ]);
         }
     }

    /**
     * @Route("/applicant/{token}/video/interview/question", options={"expose"=true}, name="applicant_interview_question", condition="request.isXmlHttpRequest()", schemes={"http"})
     * @ParamConverter("applicant", options={"mapping": {"token": "token"}})
     */
     public function interviewQuestionAction(Request $request, Applicant $applicant)
     {
        $em = $this->getDoctrine()->getManager();
        $applicantVideo = new ApplicantVideo();
        $QuestionVideo = $applicant->getNextQuestionVideoToInterview();
        $nextQuestionVideo = $QuestionVideo['questionVideo'];
        $count = $QuestionVideo['count'];
         if (!$nextQuestionVideo instanceof QuestionVideo) {
            $applicant->setInterviewCompleted(
                Applicant::STATUS_INVITED_YES
            );

            $em->flush();

            return new JsonResponse([
                'success' => true,
                'notNextVideo' => true,
                'thankyouContent' => $this->renderView('Job/Video/thankyou.html.twig')
            ]);

            }


        if ($request->files->get('applicant_video')) {
            $ajax_applicant_video = $request->files->get('applicant_video');
            $video_file = $ajax_applicant_video['videoFile'];
            $rsApplicantVideo = $request->request->get('applicant_video');
            $writtenAnswer = isset($rsApplicantVideo['writtenAnswer']) ? $rsApplicantVideo['writtenAnswer'] : '';
            $applicantVideo
                ->setQuestionVideo($nextQuestionVideo)
                ->setApplicant($applicant)
                ->setVideoFile($video_file['file'])
                ->setWrittenAnswer($writtenAnswer)
            ;
            $em->persist($applicantVideo);
            $em->flush();
            $em->refresh($applicant);



        $QuestionVideo = $applicant->getNextQuestionVideoToInterview();
        $nextQuestionVideo = $QuestionVideo['questionVideo'];
        $count = $QuestionVideo['count'];

            if (!$nextQuestionVideo){
            $applicant->setInterviewCompleted(
                Applicant::STATUS_INVITED_YES
            );
            $em->flush();

            return new JsonResponse([
                'success' => true,
                'notNextVideo' => true,
                'thankyouContent' => $this->renderView('Job/Video/thankyou.html.twig')
            ]);
            }
        }

        $form = $this->createForm(new ApplicantVideoType(), $applicantVideo);
        return new JsonResponse([
            'success' => true,
            'question' => $this->renderView('Job/Video/applicant_video.html.twig',[
                'applicant' => $applicant,
                'count' => $count,
                'nextQuestionVideo' => $nextQuestionVideo,
                'form' =>$form->createView()
         ]),
        ]);
     }

    /**
     * @Route("/applicant/{token}/video/interview/thankyou", name="applicant_interview_thankyou")
     * @ParamConverter("applicant", options={"mapping": {"token": "token"}})
     */
    public function thankyouAction(Applicant $applicant)
    {
        $this->get('sylius.email_sender')->send('applicant_interview_thank_you', [
            $applicant->getEmail() ,
        ], [
            'applicant' => $applicant,
        ]);

        return $this->render('Job/Video/thankyou.html.twig', [
            'applicant' => $applicant,
        ]);
    }

    /**
     * @Route("/applicant/update-browser", options={"expose"=true}, name="browser_not_updated")
     */
    public function browerNotUpdatedAction()
    {
        return $this->render('Job/Video/browser.html.twig');
    }
}
