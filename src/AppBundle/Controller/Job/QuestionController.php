<?php

namespace AppBundle\Controller\Job;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Form\Type\QuestionType;
use AppBundle\Form\Type\QuestionVideoType;
use AppBundle\Entity\Job;
use AppBundle\Entity\Question;
use AppBundle\Entity\QuestionVideo;

/**
 * @Route("/dashboard")
 */
class QuestionController extends Controller
{
    /**
     * @Route("/job/{slug}/question/add/{questionType}", name="job_question_add", options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function addAction(Request $request, Job $job)
    {
        $questionType = strtolower($request->get('questionType'));
        if (!in_array($questionType, Question::getAllTypes())) {
            throw $this->createNotFoundException();
        }

        if (Job::QUESTIONS_LIMIT == $job->getQuestions()->count()) {
            return new JsonResponse([
                'success' => false,
                'title' => $this->get('translator')->trans('ajax.error.limit_question_reached'),
                'messages' => [],
            ]);
        }

        $question = new Question();
        $question
            ->setJob($job)
            ->setType($questionType)
            ->setOrder(
                $job->getQuestions()->count() + 1
            )
        ;

        $form = $this->createForm(new QuestionType(), $question);

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $response = ['success' => false];

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if (!$question->getId()) {
                    $em->persist($question);
                }

                $em->flush();
                $em->refresh($job);

                $response = [
                    'success' => true,
                    'title' => $this->get('translator')->trans('ajax.success.question_added'),
                    'messages' => [],
                    'questions_list_view' => $this->renderView('Job/Question/_list.html.twig', [
                        'job' => $job,
                    ]),
                    'questions_remaining' => $job->getQuestionsRemaining(),
                ];

            } else {
                $response['errors'] = $this->get('form.errors')->getFormErrors($form);
            }

            return new JsonResponse($response);
        }

        return $this->render('Job/Question/add.html.twig', [
            'form' => $form->createView(),
            'job' => $job,
            'question' => $question,
            'questionType' => $questionType,
        ]);
    }

    /**
     * @Route("/job/{slug}/question/video/add", name="job_question_video_add", options = { "expose" = true })
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function videoAddAction(Request $request, Job $job)
    {

        $questionVideo =  new QuestionVideo();

        $form = $this->createForm(new QuestionVideoType(), $job);
        $form->handleRequest($request);
     
        $em = $this->getDoctrine()->getManager();
        if ($request->isXmlHttpRequest()) { 
            $questionVideo->setJob($job);             
            $questionVideo->setOrder(5);             
            $questionVideo->setQuestion($request->request->get('question'));             
            $questionVideo->setDuration($request->request->get('duration'));             
            $questionVideo->setTimeReadQuestion($request->request->get('timeReadQuestion'));   
            $em->persist($questionVideo);
            $em->flush();
            
            $vewForm = $this->createForm(new QuestionVideoType(), $job);
            $listQuestionsVideo = $this->renderView('Job/Video/_list_question.html.twig', [
            'formQuestionVideo' => $vewForm->createView(),
             ]);
        
        return new JsonResponse([
            'success' => true,
            'listQuestionsVideo' => $listQuestionsVideo,
            'count' => count($job->getQuestionsVideo()),
            'message' => 'Goood',
            ]);
        }
       
        return $this->render('Job/Step/modal_create_video.html.twig', [
            'formQuestionVideo' => $form->createView(),
            'job' => $job,
        ]);
        
    }
     /**
     * @Route("/job/{slug}/question/video/delete/{id}", name="question_video_delete", options={"expose"=true})
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("QuestionVideo", options={"mapping": {"id": "id"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function questionVideoDeleteAction(Request $request, Job $job, QuestionVideo $questionVideo)
    {
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($questionVideo);
        $em->flush();

        $form = $this->createForm(new QuestionVideoType(), $job);
        $listQuestionsVideo = $this->renderView('Job/Video/_list_question.html.twig', [
            'formQuestionVideo' => $form->createView(),
        ]);
        return new JsonResponse([
            'success' => true,
            'listQuestionsVideo' => $listQuestionsVideo,
            'count' => count($job->getQuestionsVideo()),
            'message' => 'Goood',
            ]);
    }

    /**
     * @Route("/job/{slug}/question/video/order", name="job_question_video_order", options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function orderQuestionVideoAction(Request $request, Job $job)
    {
        
        $em = $this->getDoctrine()->getManager();
        $order = $request->request->get('order');
        foreach ($order as $key => $item) {
            $questionVideo = $em->getRepository('AppBundle:QuestionVideo')->findOneBy([
                'id' => $item
            ]);
            $questionVideo->setOrder($key+1);

        }
        $em->persist($questionVideo);
        $em->flush();

        $form = $this->createForm(new QuestionVideoType(), $job);
        $listQuestionsVideo = $this->renderView('Job/Video/_list_question.html.twig', [
            'formQuestionVideo' => $form->createView(),
        ]);
         return new JsonResponse([
            'success' => true,
            'newList' => $listQuestionsVideo,
            'message' => 'Ok!!!',
            ]);
    }

    /**
     * @Route("/job/{slug}/question/edit/{questionType}/{questionId}", name="job_question_edit", options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("question", options={"mapping": {"questionId": "id"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function editAction(Request $request, Job $job, Question $question)
    {
        $questionType = strtolower($request->get('questionType'));
        if (!in_array($questionType, Question::getAllTypes())) {
            throw $this->createNotFoundException();
        }

        if (in_array($questionType, Question::getMultipleChoicesTypes())) {
            $originalAnswers = new ArrayCollection();
            foreach ($question->getAnswers() as $answer) {
                $originalAnswers->add($answer);
            }
        }

        $form = $this->createForm(new QuestionType(), $question);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $response = ['success' => false];

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                if (in_array($questionType, Question::getMultipleChoicesTypes())) {
                    foreach ($originalAnswers as $answer) {
                        if (false === $question->getAnswers()->contains($answer)) {
                            $question->removeAnswer($answer);
                            $em->remove($answer);
                        }
                    }
                }

                if (!$question->getId()) {
                    $em->persist($question);
                }

                $em->flush();

                $response = [
                    'success' => true,
                    'title' => $this->get('translator')->trans('ajax.success.question_edited'),
                    'messages' => [],
                    'questions_list_view' => $this->renderView('Job/Question/_list.html.twig', [
                        'job' => $job,
                    ]),
                    'questions_remaining' => $job->getQuestionsRemaining(),
                ];

            } else {
                $response['errors'] = $this->get('form.errors')->getFormErrors($form);
            }

            return new JsonResponse($response);
        }

        return $this->render('Job/Question/edit.html.twig', [
            'form' => $form->createView(),
            'job' => $job,
            'question' => $question,
            'questionType' => $questionType,
        ]);
    }

    /**
     * @Route("/job/{slug}/question/delete/{questionId}", name="job_question_delete", options={"expose"=true})
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("question", options={"mapping": {"questionId": "id"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function deleteAction(Request $request, Job $job, Question $question)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($question);
        $em->flush();

        $em->refresh($job);
        foreach ($job->getQuestions() as $keyItem => $item) {
            $item->setOrder($keyItem + 1);
        }

        $em->flush();

        return new JsonResponse([
            'success' => true,
            'title' => $this->get('translator')->trans('ajax.success.question_deleted'),
            'messages' => [],
            'questions_list_view' => $this->renderView('Job/Question/_list.html.twig', [
                'job' => $job,
            ]),
            'questions_remaining' => $job->getQuestionsRemaining(),
        ]);
    }

    /**
     * @Route("/job/{slug}/question/order", name="job_question_order", options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function orderAction(Request $request, Job $job)
    {
        $em = $this->getDoctrine()->getManager();
        $order = $request->request->get('order');
        foreach ($order as $key => $item) {
            $question = $em->getRepository('AppBundle:Question')->findOneBy([
                'id' => $item
            ]);
            $question->setOrder($key+1);

        }
        $em->persist($question);
        $em->flush();

        $newList = $this->renderView('Job/Question/_list.html.twig', [
            'job' => $job,
        ]);
         return new JsonResponse([
            'success' => true,
            'newList' => $newList,
            'message' => 'Ok!!!',
            ]);
    }

    /**
     * @Route("/job/{slug}/question/video/preview", name="job_preview_question_video", options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
     public function previewQuestionVideoAction(Request $request, Job $job)
     {
         $questions = $this->renderView('Job/Question/_list_video_preview.html.twig', [
            'job' => $job,
        ]);
            return new JsonResponse([
            'success' => true,
            'questionsVideo' => $questions,
            ]);
     }

     /**
     * @Route("/job/{slug}/question/preview", name="job_preview_question", options={"expose"=true}, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
     public function previewQuestionAction(Request $request, Job $job)
     {
            return new JsonResponse([
            'success' => true,
            'questions' => $this->renderView('Job/Question/preview.html.twig', [
                            'job' => $job,
            ])
            ]);

     }

     /**
     * @Route("/question-video/{id}/update", name="update_question_video", options={"expose"=true})
     * @ParamConverter("job", options={"mapping": {"id": "id"}})
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function questionVideoUpdateAction(Request $request, Job $job)
    {
        $form = $this->createForm(new QuestionVideoType, $job);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) { 
                $em = $this->getDoctrine()->getManager();
                $em->flush();
        }

        return new JsonResponse([
                'success' => true,
        ]);

    }



}
