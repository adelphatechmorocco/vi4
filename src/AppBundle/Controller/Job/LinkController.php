<?php

namespace AppBundle\Controller\Job;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Job;
use AppBundle\Entity\JobLink;

class LinkController extends Controller
{

     /**
     * @Route("/job/{id}/add-new-link", name="job_add_shortner", options={"expose"=true})
     * @ParamConverter("job", options={"mapping": {"id": "id"}})
     */
    public function addAction(Request $request, Job $job)
    {
        if (!$request->isXmlHttpRequest() || !$request->request->has('label')) {
            throw $this->createNotFoundException();
        }

        $label = $request->request->get('label');
        $link = new JobLink();

        $link
            ->setLabel($label)
            ->setJob($job)
        ;

        $em = $this->getDoctrine()->getManager();
        $em->persist($link);
        $em->flush();

        return new JsonResponse([
            'success' => true,
            'slug' => $link->getSlug(),
            'shortener' => $this->generateUrl(
                'job_shortner',
                [
                    'token' => $job->getToken(),
                    'slug' => $link->getSlug(),
                ],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
        ]);
    }

    /**
     * @Route("/{token}/{slug}" , name="job_shortner")
     * @ParamConverter("job", options={"mapping": {"token": "token"}})
     */
    public function displayAction(Request $request, Job $job)
    {
        $em = $this->getDoctrine()->getManager();

        $source = null;
        if (!array_key_exists($request->get('slug'), JobLink::DEFAULT_LINKS())) {
            $jobLink = $em->getRepository('AppBundle:JobLink')->findByTokenAndSlug(
                $request->get('token'),
                $request->get('slug')
            );

            if (!$jobLink) {
                throw $this->createNotFoundException();
            }

            $source = $jobLink->getLabel();

        } else {
           $source = JobLink::DEFAULT_LINKS()[$request->get('slug')];
        }

        $request->getSession()->set('applicant_source', $source);

        return $this->redirectToRoute('job_preview', [
            'slug' => $job->getSlug(),
        ]);
    }
}
