<?php

namespace AppBundle\Controller\Job;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\JobRequiredDocumentsType;
use AppBundle\Entity\Job;
use AppBundle\Entity\JobDocument;
use AppBundle\Entity\Question;

/**
 * @Route("/dashboard")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/job/create", name="job_create")
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $job = new Job();
        $job->setOwner($this->getUser()->getCompany());
        $flow = $this->get('app.form.flow.job');
        $flow->bind($job);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            $em->persist($job);
            $em->flush();

            return $this->redirectToRoute('job_edit', [
                'id' => $job->getId(),
                'slug' => $job->getSlug(),
                'created' => '',
            ]);
        }

        return $this->render('Job/Step/index.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
        ]);
    }

    /**
     * @Route("/job/{id}-{slug}", name="job_edit")
     * @ParamConverter("job", options={"mapping": {"id": "id"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function editAction(Request $request, Job $job)
    {
        $links = ($request->query->get('links')) ? true : false;
       
        if(count($job->getApplicants()) > 0 && !$links):
           return $this->redirectToRoute('jobs_list');
        endif;

        $em = $this->getDoctrine()->getManager();
        $slug = $job->getSlug();
        $flow = $this->get('app.form.flow.job');
        $flow->bind($job);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $currentStep = $flow->getCurrentStepNumber();
            $flow->saveCurrentStepData($form);
            $em->flush();
            
            if (3 === $flow->getCurrentStepNumber()) {
                $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
                $documentPath = $this->get('kernel')->getRootDir() . '/../web' . $helper->asset($job, 'companyLogoFilePreview');

                if ($this->get('filesystem')->exists($documentPath)) {
                    $moveToPath = '/'.trim(str_replace('/tmp', '/', dirname($documentPath)), '/');
                    if($job->getCompanyLogoFilePreview() && $job->getCompanyLogoPreview()) {
                         $job->getCompanyLogoFilePreview()->move($moveToPath, $job->getCompanyLogoPreview());
                    }
                }

                $job
                    ->setCompanyLogo($job->getCompanyLogoPreview())
                    ->setBackgroundTexture($job->getBackgroundTexturePreview())
                    ->setBackgroundColor($job->getBackgroundColorPreview())
                    ->setCompanyLogoPreview(null)
                ;
            }


            $em->flush();

            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                $flow->reset();

                $this->addFlash(
                    'success',
                    'flash.message.success.job.saved'
                );

                return $this->redirectToRoute('jobs_list');
            }
        }

        return $this->render('Job/Step/index.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
            'job' => $job,
        ]);
    }

    /**
     * @Route("/job/{slug}/required-documents", name="job_required_documents")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function requiredDocumentsAction(Request $request, Job $job)
    {
        $formRequiredDocuments = $this->createForm(new JobRequiredDocumentsType(), null, [
            'job' => $job
        ]);

        $formRequiredDocuments->handleRequest($request);

        if ($request->isXmlHttpRequest() && $request->isMethod('POST')) {
            if ($formRequiredDocuments->isValid()) {
                $em = $this->getDoctrine()->getManager();

                foreach ($job->getDocuments() as $document) {
                    // $job->removeDocument($document);
                    $em->remove($document);
                }

                foreach ($formRequiredDocuments['documentType']->getData() as $documentType) {
                    $jobDocument = new JobDocument();
                    $jobDocument
                        ->setDocumentType($documentType)
                        ->setName(JobDocument::DOCUMENT_TYPE_OTHER === $documentType ? $formRequiredDocuments['name']->getData() : null)
                    ;

                    $job->addDocument($jobDocument);
                }

                $em->flush();

                $response = [
                    'success' => true,
                    'title' => $this->get('translator')->trans('ajax.success.required_documents'),
                    'messages' => [],
                ];
            } else {
                $response['errors'] = $this->get('form.errors')->getFormErrors($form);
            }

            return new JsonResponse($response);
        }

        return $this->render('Job/Step/_required_documents.html.twig', [
            'formRequiredDocuments' => $formRequiredDocuments->createView(),
            'job' => $job,
        ]);
    }
}
