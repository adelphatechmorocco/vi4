<?php

namespace AppBundle\Controller\Job;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\JobThirdStepType;
use AppBundle\Entity\Job;
use AppBundle\Entity\Applicant;
use AppBundle\Entity\Answer;
use AppBundle\Entity\ApplicantDocument;
use AppBundle\Entity\BackgroundTexture;
use AppBundle\Form\Type\ApplicantType;
use OpauthBundle\Profile;

class PreviewController extends Controller
{
    /**
     * @Route("/{slug}", name="job_preview")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     */
    public function indexAction(Request $request, Job $job, $preview = false)
    {
        if (!$job->isOpen()) {
            return $this->render('Job/closed_job.html.twig', [
                'job' => $job,
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $applicant = new Applicant();
        $applicant
            ->setJob($job)
            ->setSource(
                $request->getSession()->get('applicant_source')
            )
        ;

        foreach ($job->getQuestions() as $question) {
            $answer = new Answer();
            $answer
                ->setQuestion($question)
            ;

            $applicant->addAnswer($answer);
        }

        foreach ($job->getDocuments() as $document) {
            $applicantDocument = new ApplicantDocument();
            $applicantDocument
                ->setJobDocument($document)
            ;

            $applicant->addDocument($applicantDocument);
        }

        $form = $this->createForm(new ApplicantType, $applicant, [
            'profile' => $request->getSession()->get('user_profile') ?: new Profile(),
        ]);

        $request->getSession()->remove('user_profile');

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($preview) {
                $this->addFlash('notice', 'flash.message.notice.job.preview_mode');

                return $this->redirect($request->getUri());
            }

            $em->persist($applicant);
            $em->flush();

            $this->get('sylius.email_sender')->send(
                'applicant_job_apply',
                [$applicant->getEmail()],
                [
                    'applicant' => $applicant,
                    'job' => $job,
                ]
            );

            $request->getSession()->remove('applicant_source');

            $this->addFlash('success', 'applicant.alert.saved.success');

            return $this->redirectToRoute('applicant_job_thankyou');
        }

        return $this->render('Job/preview.html.twig', [
            'form' => $form->createView(),
            'job' => $job,
            'preview' => $preview,
            'request' => $preview ? $request : null,
        ]);
    }

    /**
     * @Route("/dashboard/job/{slug}/preview", name="job_preview_test", options={"expose"=true})
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function testAction(Request $request, Job $job)
    {
        $form = $this->createForm(new JobThirdStepType(), $job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->forward('AppBundle:Job\Preview:index', [
            'request'  => $request,
            'job'      => $job,
            'preview'  => true,
        ]);
    }

    /**
     * @Route("/applicant/thankyou", name="applicant_job_thankyou")
     */
      public function thankYouApplicantAction(Request $request)
      {
           return $this->render('Applicant/thank_you.html.twig');
      }


    /**
     * @Route("/uploads/applicants/documents/{documentName}", name="job_applicant_document_absolute_url")
     * @ParamConverter("applicantDocument", options={"mapping": {"documentName": "document"}})
     * Security("has_role('ROLE_EMPLOYER') and is_granted('edit', job)")
     */
    public function absoluteUrlDocumentAction(Request $request, ApplicantDocument $applicantDocument)
    {
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $documentPath = $this->get('kernel')->getRootDir() . '/../web' . $helper->asset($applicantDocument, 'documentFile');

        if (!$this->get('filesystem')->exists($documentPath)) {
            throw $this->createNotFoundException();
        }
    
        
        $response = new BinaryFileResponse($documentPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $this->render('Dashboard/Job/document_viewer.html.twig', [
            'applicantDocument' => $response,
            'documentPath' => $documentPath
        ]);
       
    }
}
