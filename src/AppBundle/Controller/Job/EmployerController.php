<?php

namespace AppBundle\Controller\Job;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Job;
use AppBundle\Entity\Applicant;
use AppBundle\Entity\ApplicantDocument;

/**
 * @Route("/dashboard")
 */
class EmployerController extends Controller
{
    /**
     * @Route("/jobs", name="jobs_list")
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Job')->listQuery(
            $this->getUser()->getCompany()
        );

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('Dashboard/Job/list.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/job/{slug}/duplicate", name="duplicate_job")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function duplicateJobAction(Request $request, Job $job)
    {
        $em = $this->getDoctrine()->getManager();
        $questions = $job->getQuestions();
        $newJob = clone $job;
        $newJob->_clone();    
        $em->persist($newJob);
        $em->flush();
         return $this->redirectToRoute('jobs_list');
    }

    /**
     * @Route("/job/{slug}/change-status", name="job_change_status", options = { "expose" = true }, condition="request.isXmlHttpRequest()")
     * @ParamConverter("job", options={"mapping": {"slug": "slug"}})
     * @Security("has_role('ROLE_EMPLOYER')")
     */
    public function openCloseAction(Request $request, Job $job)
    {
        $status = Job::STATUS_JOB_OPEN;
        if (Job::STATUS_JOB_OPEN === $job->getStatus()) {
            $status = Job::STATUS_JOB_CLOSE;
        }

        $em = $this->getDoctrine()->getManager();
        $job->setStatus($status);
        $em->persist($job);
        $em->flush();

        if (!$job->isOpen()) {
            $rejectedApplicants = $em->getRepository('AppBundle:Applicant')->findRejectedApplicants();
            foreach ($rejectedApplicants as $rejectedApplicant) {
                $this->get('sylius.email_sender')->send('applicant_rejection_advanced_no', [
                    $rejectedApplicant->getEmail() ,
                ], [
                    'applicant' => $rejectedApplicant,
                ]);
            }
        }

        return new JsonResponse([
            'success' => true,
            'title' => $this->get('translator')->trans('flash.message.success.job.status_changed'),
            'messages' => [],
        ]);
    }


}
