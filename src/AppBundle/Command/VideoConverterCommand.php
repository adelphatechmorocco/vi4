<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use AppBundle\Entity\ApplicantVideo;

class VideoConverterCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        
            ->setName('haystack:video:convert')
            ->setDescription('Convert video applicant')
            
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $applicantVideo = new ApplicantVideo();
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        $videos =  $em
        ->getRepository('AppBundle:ApplicantVideo')
        ->findBy([
            'converted' => ApplicantVideo::VIDEO_NOT_CONVERTED,
        ]    
        );

         
        $helper = $this->getContainer()->get('vich_uploader.templating.helper.uploader_helper');
        
            foreach($videos as $video){
                if (!$video->getVideoFile()->isFile() ) {
                    continue;
                }

                $videoPath = $this->getContainer()->get('kernel')->getRootDir() . '/../web' . $helper->asset($video, 'videoFile');
                $process = new Process(
                    sprintf(
                        'cd "%s" && ffmpeg -i "%s" "%s" ',
                        dirname($video->getVideoFile()->getRealPath()),
                        $videoPath,
                        str_replace(
                            $video->getVideoFile()->getExtension(), 
                            'mp4', 
                            $video->getVideoFile()->getBasename()
                        )
                    ));
                $process->setTimeout(null);
                $process->run(function ($type, $buffer) {
                    if (Process::ERR === $type) {
                        echo 'ERR > '.$buffer;
                    } else {
                        echo 'OUT > '.$buffer;
                    }
                });
                 $videoMp4Path =  str_replace($video->getVideoFile()->getExtension(), 'mp4',$videoPath);
                if (file_exists($videoMp4Path)){
                        $video->setConverted(ApplicantVideo::VIDEO_CONVERTED);
                    }
        }        
        $em->flush();
    }
}