<?php

namespace AppBundle\Form\Filter;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use AppBundle\Entity\Question;

class ApplicantQuestionListingFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('questions', Type\CollectionType::class, [
                'label' => false,
                'entry_type' => QuestionListingFilterType::class,
                'entry_options' => [
                    'label' => false,
                ],
            ])
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                function (FormEvent $event) {
                    $data = $event->getData();
                    $form = $event->getForm();

                    $form
                        ->add('page', Type\HiddenType::class, [
                            'mapped' => false,
                        ])
                    ;
                }
            )
        ;
    }

    public function getBlockPrefix()
    {
        return 'af';
    }

    public function getName()
    {
        return 'applicant_listing_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Job',
            'csrf_protection'   => false,
            'validation_groups' => ['filtering'],
            'method' => 'GET',
        ]);
    }
}
