<?php

namespace AppBundle\Form\Filter;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use AppBundle\Entity\Question;

class QuestionListingFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    $question = $event->getData();
                    $form = $event->getForm();

                    if (!in_array($question->getType(), Question::getMultipleChoicesTypes())) {
                        return;
                    }

                    if ($question->getType() === Question::TYPE_DROPDOWN) {
                        $form
                            ->add('a', Filters\ChoiceFilterType::class, [
                                'label' => false,
                                'mapped' => false,
                                'choices' => $question->getAnswers(),
                                'placeholder' => 'form.applicant.answers_metadata.placeholder',
                                'attr' => [
                                    'class' => '-inline',
                                ],
                            ])
                        ;
                    }

                    if ($question->getType() === Question::TYPE_RADIO) {
                        $form
                            ->add('a', Filters\ChoiceFilterType::class, [
                                'label' => false,
                                'mapped' => false,
                                'choices' => $question->getAnswers(),
                                'expanded' => true,
                                'multiple' => false,
                                'placeholder' => false,
                                'attr' => [
                                    'class' => 'radio-inline',
                                ],
                                'label_attr' => [
                                    'class' => 'label-radio radio-inline',
                                ],
                            ])
                        ;
                    }

                    if ($question->getType() === Question::TYPE_CHECKBOX) {
                        $form
                            ->add('a', Filters\ChoiceFilterType::class, [
                                'label' => false,
                                'mapped' => false,
                                'choices' => $question->getAnswers(),
                                'expanded' => true,
                                'multiple' => true,
                                'placeholder' => false,
                                'attr' => [
                                    'class' => 'checkbox-inline',
                                ],
                                'label_attr' => [
                                    'class' => 'label-checkbox',
                                ],
                            ])
                        ;
                    }
                }
            )
        ;
    }

    public function getBlockPrefix()
    {
        return 'af';
    }

    public function getName()
    {
        return 'applicant_listing_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Question',
            'csrf_protection'   => false,
            'validation_groups' => ['filtering'],
            'method' => 'GET',
        ]);
    }
}
