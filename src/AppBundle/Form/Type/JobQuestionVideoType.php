<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type;
use AppBundle\Entity\QuestionVideo;


class JobQuestionVideoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', null, [
                'label' => false,
            ])
            ->add('duration', Type\ChoiceType::class, [
                'label' => false,
                'choices' => [
                    '10 sec' => QuestionVideo::DURATION_10_SECONDS,
                    '30 sec' => QuestionVideo::DURATION_30_SECONDS,
                    '1 min' => QuestionVideo::DURATION_60_SECONDS,
                    '1:30 min' => QuestionVideo::DURATION_90_SECONDS,
                    '2 mins' => QuestionVideo::DURATION_120_SECONDS,
                    '2:30 mins' => QuestionVideo::DURATION_150_SECONDS,
                    '3 mins' => QuestionVideo::DURATION_180_SECONDS,
                ],
            'choices_as_values' => true,
            'choice_value' => function ($choice) {
            return $choice;
            },
            ])
            ->add('timeReadQuestion', Type\ChoiceType::class, [
                'label' => false,
                'choices' => [
                    '5 sec' => QuestionVideo::TIME_READ_QUESTION_5,
                    '10 sec' => QuestionVideo::TIME_READ_QUESTION_10,
                    '15 sec' => QuestionVideo::TIME_READ_QUESTION_15,
                    '20 sec' => QuestionVideo::TIME_READ_QUESTION_20,
                ],
            'choices_as_values' => true,
            'choice_value' => function ($choice) {
            return $choice;
            },
            ])  
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\QuestionVideo',
        ));
    }
}
