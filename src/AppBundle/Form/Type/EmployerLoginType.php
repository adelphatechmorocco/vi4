<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;

class EmployerLoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', Type\EmailType::class, [
                'label' => 'form.login._username.label',
            ])
            ->add('_password', Type\PasswordType::class, [
                'label' => 'form.login._password.label',
            ])
        ;
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
