<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployerAccountBillingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, [
                'label' => 'form.register.account_billing.firstname.label',
                'data' => $options['profile']->getFirstname(),
            ])
            ->add('lastname', null, [
                'label' => 'form.register.account_billing.lastname.label',
                'data' => $options['profile']->getLastname(),
            ])
            ->add('phone', null, [
                'label' => 'form.register.account_billing.phone.label',
            ])
            ->add('company', new CompanyType(), [
                'pricingModel' => $options['pricingModel'],
                'profile' => $options['profile'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Employer',
        ]);

        $resolver->setRequired([
            'pricingModel',
            'profile',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_user_account_billing';
    }
}
