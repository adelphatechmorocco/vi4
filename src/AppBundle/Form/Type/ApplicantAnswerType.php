<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Question;

class ApplicantAnswerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function ($event) {
                    $form = $event->getForm();
                    $data = $event->getData();
                    if ($data->getQuestion()->getType() === Question::TYPE_PARAGRAPH) {
                        $form
                            ->add('answers_metadata', Type\TextareaType::class, [
                                'attr' => [
                                    'rows' => 6,
                                    'class' => '-inline',
                                ]
                            ])
                        ;
                    }

                    if ($data->getQuestion()->getType() === Question::TYPE_SHORT_ANSWER) {
                        $form
                            ->add('answers_metadata', Type\TextType::class, [
                                'constraints' => [
                                   new Assert\Length([
                                        'min' => $data->getQuestion()->getQuestionMetadata()['minLength'],
                                        'max' => $data->getQuestion()->getQuestionMetadata()['maxLength'],
                                    ])
                                ],
                            ])
                        ;
                    }

                    if ($data->getQuestion()->getType() === Question::TYPE_DROPDOWN) {
                        $form
                            ->add('answers_metadata', Type\ChoiceType::class, [
                                'choices' => $data->getQuestion()->getAnswers(),
                                'placeholder' => 'form.applicant.answers_metadata.placeholder',
                                'attr' => [
                                    'class' => '-inline',
                                ]
                            ])
                        ;
                    }

                    if ($data->getQuestion()->getType() === Question::TYPE_RADIO) {
                        $form
                            ->add('answers_metadata', Type\ChoiceType::class, [
                                'choices' => $data->getQuestion()->getAnswers(),
                                'expanded' => true,
                                'multiple' => false,
                                'required' => true,
                                'placeholder' => 'form.applicant.answers_metadata.placeholder',
                                'attr' => [
                                    'class' => 'radio-inline',
                                ],
                                'label_attr' => [
                                    'class' => 'label-radio radio-inline',
                                ],
                            ])
                        ;
                    }

                    if ($data->getQuestion()->getType() === Question::TYPE_CHECKBOX) {
                        $form
                            ->add('answers_metadata', Type\ChoiceType::class, [
                                'choices' => $data->getQuestion()->getAnswers(),
                                'expanded' => true,
                                'multiple' => true,
                                'required' => true,
                                'placeholder' => 'form.applicant.answers_metadata.placeholder',
                                'attr' => [
                                    'class' => 'checkbox-inline',
                                ],
                                'label_attr' => [
                                    'class' => 'label-checkbox',
                                ],
                            ])
                        ;
                    }
                }
            )
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Answer'
        ));
    }
}
