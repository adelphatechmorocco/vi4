<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type;

class QuestionVideoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('questionsVideo', Type\CollectionType::class, [
                    'entry_type' => JobQuestionVideoType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'prototype' => true,
                    'label' => false,
                    'options' => [
                        'label' => false,
                    ],
          ])
            ->addEventListener(
                FormEvents::SUBMIT,
                 function ($event) {
                    foreach ($event->getData()->getQuestionsVideo() as $job) {
                        $job->setJob($event->getData());
                    }
                }
            )
            
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Job',
        ));
    }
}
