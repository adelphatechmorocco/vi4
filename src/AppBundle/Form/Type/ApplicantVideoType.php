<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Vich\UploaderBundle\Form\Type\VichFileType;
use AppBundle\Entity\ApplicantVideo;

class ApplicantVideoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('videoFile', VichFileType::class,[
                'required' => false
            ])
            ->add('writtenAnswer', Type\TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => 6,
                ]
            ])
            ;
    } 
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ApplicantVideo'
        ));
    }
}