<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmployerRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('username');
        $builder->remove('email');

        $builder
            ->add('email', Type\RepeatedType::class, [
                'type' => Type\EmailType::class,
                'options' => [
                    'validation_groups' => 'Registration',
                ],
                'first_options' => [
                    'label' => 'form.register.email.label',
                ],
                'second_options' => [
                    'label' => 'form.register.email_confirm.label',
                ],
                'invalid_message' => 'form.register.email.mismatch',
            ])
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}
