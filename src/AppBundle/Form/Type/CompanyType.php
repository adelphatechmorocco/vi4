<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use AppBundle\Entity\Company;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'form.register.company.name.label',
            ])
            ->add('city', null, [
                'label' => 'form.register.company.city.label',
                'data' => $options['profile']->getLocation(),
            ])
            ->add('phone', null, [
                'label' => 'form.register.company.phone.label',
            ])
            ->add('zipcode', null, [
                'label' => 'form.register.company.zipcode.label',
            ])
            ->add('paymentType', Type\ChoiceType::class, [
                'label' => 'form.register.company.paymentType.label',
                'choices' => Company::getPaymentTypeChoice(),
                'expanded' => true,
            ])
            ->add('pricingModel', null, [
                'label' => 'form.register.company.pricingModel.label',
                'data' => $options['pricingModel'],
            ])
            ->add('creditCard', new CreditCardType())
            ->add('paypalAccount', Type\EmailType::class, [
                'required' => false,
                'label' => 'form.register.company.paypalAccount.label',
            ])
            /*->add('cardNotcharged30days', Type\CheckboxType::class, [
                'mapped' => false,
                'label' => 'form.register.company.cardNotcharged30days.label',
            ])*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Company',
            'pricingModel' => null,
        ]);

        $resolver->setRequired([
            'pricingModel',
            'profile',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'company';
    }
}
