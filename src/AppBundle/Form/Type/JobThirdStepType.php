<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;

class JobThirdStepType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyLogoFilePreview', Type\FileType::class, [
                'required' => false,
                'label' => false,
            ])
            ->add('backgroundTexturePreview', null, [
                'label' => false,
                'required' => false,
                'expanded' => true,
                'empty_value' => false,
            ])
            ->add('backgroundColorPreview', Type\HiddenType::class, [
                'label' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'create_job_setup_branding';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Job',
            'validation_groups' => ['JobThirdStep'],
        ]);
    }
}
