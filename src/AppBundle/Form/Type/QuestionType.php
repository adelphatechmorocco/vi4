<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormEvents;
use AppBundle\Entity\Question;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', Type\HiddenType::class)
            ->add('question', null, [
                'label' => 'form.job.question.question.label',
            ])
            ->add('candidateInstruction', null, [
                'label' => 'form.job.question.candidateInstruction.label',
                'required' => false
            ])
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function ($event) {
                    $form = $event->getForm();
                    $data = $event->getData();

                    if ($data->getId()) {
                        $form
                            ->add('id', Type\HiddenType::class)
                        ;
                    }

                    if (in_array($data->getType(), Question::getMultipleChoicesTypes())) {
                        $form
                            ->add('answers', Type\CollectionType::class, [
                                'entry_type' => QuestionAnswerType::class,
                                'label' => 'form.job.question.answers.label',
                                'allow_add' => true,
                                'allow_delete' => true,
                                'prototype' => true,
                                'by_reference' => false,
                                'options' => [
                                    'label' => false,
                                ],
                                'attr' => [
                                    'class' => 'collection',
                                ],
                            ])
                        ;
                    }

                    if ($data->getType() === Question::TYPE_SHORT_ANSWER) {
                        $questionMetadata = $data->getQuestionMetadata();

                        $form
                            ->add('exampleAnswer', Type\TextType::class, [
                                'mapped' => false,
                                'required' => false,
                                'label' => 'form.job.question.exampleAnswer.label',
                                'data' => isset($questionMetadata['exampleAnswer']) ? $questionMetadata['exampleAnswer'] : null,
                            ])
                            ->add('minLength', Type\TextType::class, [
                                'mapped' => false,
                                'label' => 'form.job.question.minLength.label',
                                'data' => isset($questionMetadata['minLength']) ? $questionMetadata['minLength'] : 0,
                            ])
                            ->add('maxLength', Type\TextType::class, [
                                'mapped' => false,
                                'label' => 'form.job.question.maxLength.label',
                                'data' => isset($questionMetadata['maxLength']) ? $questionMetadata['maxLength'] : 600,
                            ])
                        ;
                    }
                }
            )
            ->addEventListener(
                FormEvents::SUBMIT,
                function ($event) {
                    $form = $event->getForm();
                    $question = $event->getData();

                    if ($question->getType() === Question::TYPE_SHORT_ANSWER) {
                        $questionMetadata = $question->getQuestionMetadata();
                        $questionMetadata['exampleAnswer'] = $form['exampleAnswer']->getData();
                        $questionMetadata['minLength'] = $form['minLength']->getData();
                        $questionMetadata['maxLength'] = $form['maxLength']->getData();

                        $question->setQuestionMetadata($questionMetadata);
                    }
                }
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Question',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'add_question';
    }
}
