<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use AppBundle\Entity\Job;
use AppBundle\Entity\Applicant;

class ApplicantUpdateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('note', null, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'rows' => 7,
                ],
            ])
            ->add('rate', Type\ChoiceType::class, [
                'label' => false,
                'required' => false,
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5
                ],
            ])
            ->add('advanced', Type\ChoiceType::class, [
                'label' => false,
                'empty_value' => false,
                'required' => false,
                'expanded' => true,
                'choices' => Applicant::getAdvancedChoicesLabels(),
                'label_attr' => [
                    'class' => 'label-radio',
                ],
            ])
            ->add('currentURL', Type\HiddenType::class, [
                'mapped' => false,
                'data' => $options['currentURL'],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Applicant',
        ]);

        $resolver->setRequired([
            'currentURL',
        ]);
    }
}
