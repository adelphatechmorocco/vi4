<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;

class JobLastStepType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $useFqcn = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix');
        $setChoicesAsValuesOption = $useFqcn && method_exists('Symfony\Component\Form\AbstractType', 'getName');

        $defaultChoiceOptions = array();
        if ($setChoicesAsValuesOption) {
            $defaultChoiceOptions['choices_as_values'] = true;
        }

        $builder
                ->add('links', Type\CollectionType::class, [
                    'entry_type' => LinkType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'prototype' => true,
                    'required' => false,
                    'label' => false,
                    'attr' => array(
                    'class' => 'create-new-link',
                    ),
                    'options' => [
                        'label' => false,
                    ],
                ])
                ->addEventListener(
                FormEvents::SUBMIT,
                function (FormEvent $event) {
                    foreach ($event->getData()->getLinks() as $link) {
                        $link->setJob($event->getData());
                    }
                }
            )
                ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'create_job_links';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Job',
            'validation_groups' => ['JobLastStep'],
        ]);
    }
}
