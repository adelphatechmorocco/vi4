<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use AppBundle\Entity\Question;

class ApplicantType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null,[
                'label' => false,
                'data' => $options['profile']->getFirstname(),
            ])
            ->add('lastname', null,[
                'label' => false,
                'data' => $options['profile']->getLastname(),
            ])
            ->add('email', Type\RepeatedType::class, [
                'invalid_message' => 'The field fields must match.',
                'options' => ['attr' => ['class' => 'email-field']],
                'required' => true,
                'data' => $options['profile']->getEmail(),
                'first_options'  => ['label' => false],
                'second_options' => ['label' => false],
            ])
            ->add('address', null,[
                'label' => false,
                'data' => $options['profile']->getLocation(),
            ])
            ->add('city', null,[
                'label' => false
            ])
            ->add('zipcode', null,[
                'label' => false
            ])
            ->add('phone', null,[
                'label' => false
            ])
            ->add('reffered', null,[
                'label' => false
            ])
            ->add('referralDescription', null,[
                'label' => false
            ])
            ->add('applicantPictureFile', Type\FileType::class, [
                'required' => true,
                'label' => false,
            ])
            ->add('answers', Type\CollectionType::class, [
                'entry_type' => ApplicantAnswerType::class,
                'allow_add'  => false,
                'allow_delete' => false,
                'by_reference' => false,
                'label' => false,
            ])
            ->add('documents', Type\CollectionType::class, [
                'entry_type' => ApplicantDocumentType::class,
                'allow_add'  => false,
                'allow_delete' => false,
                'by_reference' => false,
                'label' => false,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Applicant',
        ));

        $resolver->setRequired([
            'profile',
        ]);
    }
}
