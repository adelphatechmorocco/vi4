<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class CreditCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', Type\TextType::class, [
                'required' => false,
                'label' => 'form.register.credit_card.number.label',
            ])
            ->add('expirationDate', 'collot_datetime', [
                'required' => false,
                'label' => 'form.register.credit_card.expirationDate.label',
                'attr' => [
                    'data-date-format' => 'mm/yy',
                ],
                'pickerOptions' => [
                    'format' => 'MM yy',
                    'startDate' => date('January 1900'),
                    'endDate' => date('F Y'),
                ],
            ])
            ->add('name', null, [
                'required' => false,
                'label' => 'form.register.credit_card.name.label',
            ])
            ->add('csc', null, [
                'required' => false,
                'label' => 'form.register.credit_card.csc.label',
            ])
            ->add('billingCity', null, [
                'required' => false,
                'label' => 'form.register.credit_card.billingCity.label',
            ])
            ->add('zipcode', null, [
                'required' => false,
                'label' => 'form.register.credit_card.zipcode.label',
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();

                if ($data->getExpirationDate()) {
                    $expirationDate = clone $data->getExpirationDate();
                    $expirationDate->modify('last day of');
                    $data->setExpirationDate($expirationDate);
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\CreditCard',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'company_credit_card';
    }
}
