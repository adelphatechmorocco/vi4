<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormEvents;
use AppBundle\Entity\Job;
use AppBundle\Entity\JobDocument;

class JobRequiredDocumentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $job = $options['job'];

        $builder
            ->add('documentType', Type\ChoiceType::class, [
                'label' => false,
                'choices' => JobDocument::getDocumentTypeChoicesLabels(),
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('name', Type\TextType::class, [
                'disabled' => true,
            ])
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function ($event) use ($job) {
                    $form = $event->getForm();                    
                }
            )
            ->addEventListener(
                FormEvents::POST_SET_DATA,
                function ($event) use ($job) {
                    $form = $event->getForm();

                    if ($job->isDocumentTypeSelected(JobDocument::DOCUMENT_TYPE_OTHER)) {
                        $form->remove('name');
                        $form->add('name', Type\TextType::class);
                    }

                    $choices = [];
                    foreach ($job->getDocuments() as $document) {
                        if (JobDocument::DOCUMENT_TYPE_OTHER === $document->getDocumentType()) {
                            $form['name']->setData($document->getName());
                        }

                        $choices[] = $document->getDocumentType();
                    }
                    
                    $form['documentType']->setData($choices);
                }
            )
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                function ($event) use ($job) {
                    $form = $event->getForm();
                    $data = $event->getData();
                    if (isset($data['documentType'])) {
                        if (in_array(JobDocument::DOCUMENT_TYPE_OTHER, $data['documentType'])) {
                            $form->remove('name');
                            $form->add('name', Type\TextType::class);
                        }
                    }
                }
            )
            ->addEventListener(
                FormEvents::SUBMIT,
                function ($event) use ($job) {
                    $form = $event->getForm();
                    $data = $event->getData();

                }
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);

        $resolver->setRequired('job');

        $resolver->setAllowedTypes('job', 'AppBundle\Entity\Job');
    }

    public function getBlockPrefix()
    {
        return 'documents';
    }
}
