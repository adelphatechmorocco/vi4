<?php

namespace AppBundle\Form;

use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

class JobFlow extends FormFlow
{
    protected function loadStepsConfig()
    {
        return [
            [
                'label' => 'name_your_job',
                'form_type' => 'AppBundle\Form\Type\JobFirstStepType',
                'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    return $flow->getRequest()->query->has('created') || $flow->getRequest()->query->has('links');
                },
            ],
            [
                'label' => 'configure_hiring_process',
                'form_type' => 'AppBundle\Form\Type\JobSecondStepType',
                'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    return $flow->getRequest()->query->has('links');
                },
            ],
            [
                'label' => 'setup_branding',
                'form_type' => 'AppBundle\Form\Type\JobThirdStepType',
                'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    return $flow->getRequest()->query->has('links');
                },
            ],
            [
                'label' => 'post_job_links',
                'form_type' => 'AppBundle\Form\Type\JobLastStepType',
            ],
        ];
    }
}
