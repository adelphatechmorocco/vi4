<?php

namespace AppBundle\Vich\Naming;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\UniqidNamer;
use Vich\UploaderBundle\Naming\NamerInterface;

class CompanyLogoNamer extends UniqidNamer implements NamerInterface
{
    private $slugify;

    public function __construct(\Cocur\Slugify\Slugify $slugify)
    {
        $this->slugify = $slugify;
    }

    /**
     * {@inheritdoc}
     */
    public function name($object, PropertyMapping $mapping)
    {
        $uniqidName = parent::name($object, $mapping);

        return sprintf(
            '%s-%d-%s',
            $this->slugify->slugify((string) $object->getCompanyName()),
            $this->slugify->slugify((string) $object->getId()),
            $uniqidName
        );
    }
}
