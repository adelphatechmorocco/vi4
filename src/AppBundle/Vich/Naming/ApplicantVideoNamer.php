<?php

namespace AppBundle\Vich\Naming;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\UniqidNamer;
use Vich\UploaderBundle\Naming\NamerInterface;

class ApplicantVideoNamer extends UniqidNamer implements NamerInterface
{
    private $slugify;

    public function __construct(\Cocur\Slugify\Slugify $slugify)
    {
        $this->slugify = $slugify;
    }

    /**
     * {@inheritdoc}
     */
    public function name($object, PropertyMapping $mapping)
    {
      
        $uniqidName = parent::name($object, $mapping);
        return sprintf(
            '%s-%d-%s',
            $this->slugify->slugify((string) $object->getApplicant()),
            $this->slugify->slugify($object->getQuestionVideo()->getId()),
            $uniqidName
        );
    }
}
